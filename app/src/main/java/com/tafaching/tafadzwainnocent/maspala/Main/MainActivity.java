package com.tafaching.tafadzwainnocent.maspala.Main;

import android.content.Intent;
import android.os.Bundle;;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;

import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardFragment;
import com.tafaching.tafadzwainnocent.maspala.MoreDetails.MoreDetailsActivity;
import com.tafaching.tafadzwainnocent.maspala.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, BaseFragment.ParentActivityObserver {
    private ImageView img_masp;
    LinearLayout card_mas;
    TextView txt_mas,contact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        img_masp=findViewById(R.id.img_maspala);
//        img_masp.setOnClickListener(this);
//        txt_mas=findViewById(R.id.mas_name);
//        txt_mas.setOnClickListener(this);
//        contact=findViewById(R.id.contact_us);
//        contact.setOnClickListener(this);
        commit(new DashboardFragment());


    }

    @Override
    public void onClick(View v) {
        try {
//            switch (v.getId()) {
//                case R.id.img_maspala:
//                    Intent intentss = new Intent(this, DashboardActivity.class);
//                    startActivity(intentss);
//                    break;
//                case R.id.mas_name:
//                    Intent intents = new Intent(this, DashboardActivity.class);
//                    startActivity(intents);
//                    break;
//
//                case R.id.contact_us:
//                    Intent intentsx = new Intent(this, MoreDetailsActivity.class);
//                    startActivity(intentsx);
//                    break;
//
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void commit(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, baseFragment).addToBackStack(null);
        fragmentTransaction.commit();
    }
}