package com.tafaching.tafadzwainnocent.maspala.Metros;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MetroRequest implements Serializable {
    @SerializedName("getorganisations")
    private String getorganisations;

    public String getGetorganisations() {
        return getorganisations;
    }

    public void setGetorganisations(String getorganisations) {
        this.getorganisations = getorganisations;
    }
}
