package com.tafaching.tafadzwainnocent.maspala.CreateIncident;


import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesRequest;
import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesResponse;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesRequest;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesResponse;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbRequest;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbResponse;

import java.util.ArrayList;

public interface CreateIncidentContract {

    interface View {

    }

    interface CreateIncidentView {
        void onCreateIncident(Integer integer);
        void onSelectSuburb(ArrayList<SuburbResponse> response);
        void onSelectCity(ArrayList<CitiesResponse> response);
        void onSelectType(ArrayList<TypesResponse> response);


        void onError(String message);
    }

    interface Presenter {
        void createIncident(CreateIncidentRequest request, CreateIncidentView view);
        void selectSuburb(SuburbRequest request, CreateIncidentView view);
        void selectCity(CitiesRequest request, CreateIncidentView view);
        void selectType(TypesRequest request, CreateIncidentView view);

    }
}
