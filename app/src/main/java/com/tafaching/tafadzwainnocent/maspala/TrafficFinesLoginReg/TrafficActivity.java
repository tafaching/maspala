package com.tafaching.tafadzwainnocent.maspala.TrafficFinesLoginReg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardFragment;
import com.tafaching.tafadzwainnocent.maspala.R;

public class TrafficActivity extends AppCompatActivity implements  BaseFragment.ParentActivityObserver, View.OnClickListener {
    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic);
        toolbar_imgClose = findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = findViewById(R.id.toolbar_tvTitle);
        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar_imgClose.setOnClickListener(this);
        String xyz = "Traffic Fines";
        toolbar_tvTitle.setText(xyz);
        commit(new TrafficLognFragment());

    }

    @Override
    public void commit(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, baseFragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.toolbar_imageview:
                    Intent intent = new Intent(this, DashboardActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}