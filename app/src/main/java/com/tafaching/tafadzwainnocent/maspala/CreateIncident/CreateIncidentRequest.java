package com.tafaching.tafadzwainnocent.maspala.CreateIncident;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreateIncidentRequest implements Serializable {
    @SerializedName("IncidentId")
    private Integer IncidentId;
    @SerializedName("OrganisationId")
    private Integer OrganisationId;
    @SerializedName("ReferenceNumber")
    private String ReferenceNumber;
    @SerializedName("CustomerFirstName")
    private String CustomerFirstName;
    @SerializedName("CustomerSurname")
    private String CustomerSurname;
    @SerializedName("CustomerEmail")
    private String CustomerEmail;
    @SerializedName("CustomerComment")
    private String CustomerComment;
    @SerializedName("ContactNo")
    private String ContactNo;
    @SerializedName("GenderId")
    private Integer GenderId;
    @SerializedName("Gender")
    private String Gender;
    @SerializedName("Id_Passport")
    private String Id_Passport;
    @SerializedName("PlotNo")
    private String PlotNo;
    @SerializedName("Address")
    private String Address;
    @SerializedName("SuburbId")
    private Integer SuburbId;
    @SerializedName("Suburb")
    private String Suburb;
    @SerializedName("CityId")
    private Integer CityId;
    @SerializedName("City")
    private String City;
    @SerializedName("CouncilId")
    private Integer CouncilId;
    @SerializedName("Council")
    private String Council;
    @SerializedName("Latitude")
    private String Latitude;
    @SerializedName("Longitude")
    private String Longitude;
    @SerializedName("IncidentTypeId")
    private Integer IncidentTypeId;
    @SerializedName("IncidentType")
    private String IncidentType;
    @SerializedName("ReportedMediumId")
    private Integer ReportedMediumId;
    @SerializedName("ReportedMedium")
    private String ReportedMedium;

    public Integer getIncidentId() {
        return IncidentId;
    }

    public void setIncidentId(Integer incidentId) {
        IncidentId = incidentId;
    }

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }

    public String getCustomerFirstName() {
        return CustomerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        CustomerFirstName = customerFirstName;
    }

    public String getCustomerSurname() {
        return CustomerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        CustomerSurname = customerSurname;
    }

    public String getCustomerEmail() {
        return CustomerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        CustomerEmail = customerEmail;
    }

    public String getCustomerComment() {
        return CustomerComment;
    }

    public void setCustomerComment(String customerComment) {
        CustomerComment = customerComment;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public Integer getGenderId() {
        return GenderId;
    }

    public void setGenderId(Integer genderId) {
        GenderId = genderId;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getId_Passport() {
        return Id_Passport;
    }

    public void setId_Passport(String id_Passport) {
        Id_Passport = id_Passport;
    }

    public String getPlotNo() {
        return PlotNo;
    }

    public void setPlotNo(String plotNo) {
        PlotNo = plotNo;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public Integer getSuburbId() {
        return SuburbId;
    }

    public void setSuburbId(Integer suburbId) {
        SuburbId = suburbId;
    }

    public String getSuburb() {
        return Suburb;
    }

    public void setSuburb(String suburb) {
        Suburb = suburb;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public Integer getCouncilId() {
        return CouncilId;
    }

    public void setCouncilId(Integer councilId) {
        CouncilId = councilId;
    }

    public String getCouncil() {
        return Council;
    }

    public void setCouncil(String council) {
        Council = council;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public Integer getIncidentTypeId() {
        return IncidentTypeId;
    }

    public void setIncidentTypeId(Integer incidentTypeId) {
        IncidentTypeId = incidentTypeId;
    }

    public String getIncidentType() {
        return IncidentType;
    }

    public void setIncidentType(String incidentType) {
        IncidentType = incidentType;
    }

    public Integer getReportedMediumId() {
        return ReportedMediumId;
    }

    public void setReportedMediumId(Integer reportedMediumId) {
        ReportedMediumId = reportedMediumId;
    }

    public String getReportedMedium() {
        return ReportedMedium;
    }

    public void setReportedMedium(String reportedMedium) {
        ReportedMedium = reportedMedium;
    }
}
