package com.tafaching.tafadzwainnocent.maspala.Wards;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WardResponse implements Serializable {
    @SerializedName("CouncillorId")
    private Integer CouncillorId;
    @SerializedName("OrganisationId")
    private Integer OrganisationId;
    @SerializedName("Organisation")
    private String Organisation;
    @SerializedName("CouncilId")
    private Integer CouncilId;
    @SerializedName("Council")
    private String Council;
    @SerializedName("WardId")
    private Integer WardId;
    @SerializedName("Ward")
    private String Ward;
    @SerializedName("ContactName")
    private String ContactName;
    @SerializedName("ContactEmail")
    private String ContactEmail;
    @SerializedName("ContactPhone")
    private String ContactPhone;
    @SerializedName("ContactMobile")
    private String ContactMobile;
    @SerializedName("CreatedBy")
    private Double CreatedBy;
    @SerializedName("ModifiedBy")
    private Double ModifiedBy;
    @SerializedName("DateCreated")
    private String DateCreated;
    @SerializedName("DateModified")
    private String DateModified;
    @SerializedName("Active")
    private Boolean Active;
    @SerializedName("CreatedByUser")
    private String CreatedByUser;
    @SerializedName("ModifiedByUser")
    private String ModifiedByUser;
    @SerializedName("Bytes")
    private String Bytes;
    @SerializedName("Pictures")
    private String Pictures;
    @SerializedName("CouncillorLibrary")
    private CouncillorLibrary councillorLibrary;

    public Integer getCouncillorId() {
        return CouncillorId;
    }

    public void setCouncillorId(Integer councillorId) {
        CouncillorId = councillorId;
    }

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }

    public String getOrganisation() {
        return Organisation;
    }

    public void setOrganisation(String organisation) {
        Organisation = organisation;
    }

    public Integer getCouncilId() {
        return CouncilId;
    }

    public void setCouncilId(Integer councilId) {
        CouncilId = councilId;
    }

    public String getCouncil() {
        return Council;
    }

    public void setCouncil(String council) {
        Council = council;
    }

    public Integer getWardId() {
        return WardId;
    }

    public void setWardId(Integer wardId) {
        WardId = wardId;
    }

    public String getWard() {
        return Ward;
    }

    public void setWard(String ward) {
        Ward = ward;
    }

    public String getContactName() {
        return ContactName;
    }

    public void setContactName(String contactName) {
        ContactName = contactName;
    }

    public String getContactEmail() {
        return ContactEmail;
    }

    public void setContactEmail(String contactEmail) {
        ContactEmail = contactEmail;
    }

    public String getContactPhone() {
        return ContactPhone;
    }

    public void setContactPhone(String contactPhone) {
        ContactPhone = contactPhone;
    }

    public String getContactMobile() {
        return ContactMobile;
    }

    public void setContactMobile(String contactMobile) {
        ContactMobile = contactMobile;
    }

    public Double getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(Double createdBy) {
        CreatedBy = createdBy;
    }

    public Double getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(Double modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateModified() {
        return DateModified;
    }

    public void setDateModified(String dateModified) {
        DateModified = dateModified;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public String getCreatedByUser() {
        return CreatedByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        CreatedByUser = createdByUser;
    }

    public String getModifiedByUser() {
        return ModifiedByUser;
    }

    public void setModifiedByUser(String modifiedByUser) {
        ModifiedByUser = modifiedByUser;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String bytes) {
        Bytes = bytes;
    }

    public String getPictures() {
        return Pictures;
    }

    public void setPictures(String pictures) {
        Pictures = pictures;
    }

    public CouncillorLibrary getCouncillorLibrary() {
        return councillorLibrary;
    }

    public void setCouncillorLibrary(CouncillorLibrary councillorLibrary) {
        this.councillorLibrary = councillorLibrary;
    }

}
