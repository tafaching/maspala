package com.tafaching.tafadzwainnocent.maspala.Wards;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WardRequest implements Serializable {
    @SerializedName("organisationId")
    private Integer organisationId;

    public Integer getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        this.organisationId = organisationId;
    }
}
