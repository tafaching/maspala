package com.tafaching.tafadzwainnocent.maspala.CreateIncident;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardFragment;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.R;

public class CreateIncidentActivity extends AppCompatActivity implements View.OnClickListener, BaseFragment.ParentActivityObserver {
RadioButton personal,incident;
    Button next, back;
    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_incident);
        personal=findViewById(R.id.personal_details);
        personal.setOnClickListener(this);
        incident=findViewById(R.id.incident_details);
        incident.setOnClickListener(this);
        toolbar_imgClose = findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = findViewById(R.id.toolbar_tvTitle);
        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar_imgClose.setOnClickListener(this);
        String xyz = "Create Incident";
        toolbar_tvTitle.setText(xyz);

        commit(new PersonalDetailsFragment());

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.personal_details:

                    if (personal.isChecked()) {
                        incident.setChecked(false);

                   commit(new PersonalDetailsFragment());
                    }
                    break;
                case R.id.incident_details:

                    if (incident.isChecked()) {
                        personal.setChecked(false);

                        commit(new IncidentDetailsFragment());
                    }
                    break;
                case R.id.toolbar_imageview:
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void commit(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, baseFragment).addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }
}
