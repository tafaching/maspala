package com.tafaching.tafadzwainnocent.maspala.TenderDetails;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.TenderList.TenderResponse;

public class TenderDetailsActivity extends Activity {

    TextView tender_id,short_descrip,long_descrip,category,posted_date,brifing_session,timedate,venue,enquiries,tel,email,document_fee;
    Button email_documents,buy_documents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tender_details);
        TenderResponse tenderResponse=(TenderResponse)getIntent().getSerializableExtra("details");

        tender_id=findViewById(R.id.tender_id);
        short_descrip=findViewById(R.id.short_description);
        long_descrip=findViewById(R.id.long_description);
        category=findViewById(R.id.category);
        posted_date=findViewById(R.id.posted_date);
        brifing_session=findViewById(R.id.briefing_session);
        timedate=findViewById(R.id.date_time);
        venue=findViewById(R.id.venue);
        enquiries=findViewById(R.id.enquiries);
        tel=findViewById(R.id.tel);
        email=findViewById(R.id.email);
        document_fee=findViewById(R.id.document_fee);

        //set data
        tender_id.setText(String.valueOf(tenderResponse.getTenderId()));
        short_descrip.setText(tenderResponse.getShortDescription());
        long_descrip.setText(tenderResponse.getDescription());
        category.setText(tenderResponse.getCategory());
        posted_date.setText(tenderResponse.getDatePosted());
        brifing_session.setText(tenderResponse.getBriefing());
       // timedate.setText(tenderResponse.getTime());
        venue.setText(tenderResponse.getVenue());
        enquiries.setText(tenderResponse.getContact());
        tel.setText(tenderResponse.getContact());
        email.setText(tenderResponse.getContact());
        document_fee.setText(tenderResponse.getDocumentPrice());





    }
}
