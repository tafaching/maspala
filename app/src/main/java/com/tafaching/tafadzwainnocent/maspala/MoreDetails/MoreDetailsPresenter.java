package com.tafaching.tafadzwainnocent.maspala.MoreDetails;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.tafaching.tafadzwainnocent.maspala.Network.Api;
import com.tafaching.tafadzwainnocent.maspala.Util.Sharepref;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MoreDetailsPresenter implements MoreDetailsContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static MoreDetailsPresenter INSTANCE;
    private Api api;

    private MoreDetailsPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    static MoreDetailsPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new MoreDetailsPresenter(context.getApplicationContext());
    }



    @Override
    public void requestOrgDetails(MoreDetailsRequest request, MoreDetailsContract.MoreDetailsView view) {

        api.getOrgDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onGetOrgDetails(response);
                }, error -> {
                    Toast.makeText(context, "Error" + error.getMessage(), Toast.LENGTH_LONG).show();
                    view.onError(null);
                });
    }
}
