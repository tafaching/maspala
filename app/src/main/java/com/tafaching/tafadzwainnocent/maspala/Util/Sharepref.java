package com.tafaching.tafadzwainnocent.maspala.Util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.tafaching.tafadzwainnocent.maspala.Metros.MetroResponse;

public class Sharepref {


    private static final String SAVE = "save";
    private static final String ID = "OrganisationId";
    private static final String NAME = "name";
    private static final String PHONE = "PhoneNumber";
    private static final String SURNAME = "surname";
    private static final String IDNUMBER = "idnumber";
    private static final String GENDER = "gender";
    private static final String ADDRESS = "address";
    private static final String GENDERID = "gender_id";



    public static void saveGenderId(Context context, Integer genderId) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(ID, genderId);
        editor.commit();
    }

    public static int getGenderId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getInt(ID, 0);
    }


    public static void saveOrganisationId(Context context, Integer organisationId) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(ID, organisationId);
        editor.commit();
    }

    public static int getOrganisationId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getInt(ID, 0);
    }

    public static void saveName(Context context, String name) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(NAME, name);
        editor.commit();
    }

    public static String getName(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getString(NAME, null);
    }

    public static void savePhoneNumber(Context context, String phone) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PHONE, phone);
        editor.commit();
    }

    public static String getPhoneNumber(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getString(PHONE, null);
    }

    public static void saveSurname(Context context, String surname) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SURNAME, surname);
        editor.commit();
    }

    public static String getSurname(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getString(SURNAME, null);
    }

    public static void saveIdnumber(Context context, String idnumber) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(IDNUMBER, idnumber);
        editor.commit();
    }

    public static String getIdnumber(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getString(IDNUMBER, null);
    }

    public static void saveAddress(Context context, String address) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ADDRESS, address);
        editor.commit();
    }

    public static String getAddress(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getString(ADDRESS, null);
    }


    public static void saveGender(Context context, String gender) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(GENDER, gender);
        editor.commit();
    }

    public static String getGender(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(SAVE, Context.MODE_PRIVATE);
        return sharedPref.getString(GENDER, null);
    }
}
