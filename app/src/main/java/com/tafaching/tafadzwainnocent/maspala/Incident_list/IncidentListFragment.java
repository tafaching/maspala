package com.tafaching.tafadzwainnocent.maspala.Incident_list;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardFragment;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncidentListFragment extends AppCompatActivity implements IncidentListContract.IncidentListView, BaseFragment.ParentActivityObserver, IncidentListAdapter.OnItemClick, View.OnClickListener {
    IncidentListPresenter presenter;
    private RecyclerView recyclerView;
    IncidentListAdapter incidentListAdapter;
    private ArrayList<IncidentListResponse> incidentListResponses;
    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;

   

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_incident_list);
        presenter = IncidentListPresenter.getInstance(this);
        incidentListResponses = new ArrayList<>();
        recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        incidentListAdapter = new IncidentListAdapter(incidentListResponses,this, this::onClickMetro);
        recyclerView.setAdapter(incidentListAdapter);
        toolbar_imgClose = findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = findViewById(R.id.toolbar_tvTitle);
        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar_imgClose.setOnClickListener(this);
        String xyz = "Incident List";
        toolbar_tvTitle.setText(xyz);
        post();

    }

    public void post() {

        String org = "0738837905";
        IncidentListRequest request = new IncidentListRequest();
        request.setMobileNo(org);
        presenter.requestIncidentList(request, this);
    }

    @Override
    public void onClickMetro(IncidentListResponse clickedResponse) {
//        Toast.makeText(this, "clicked!!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, IncidentDetailActivity.class);
        intent.putExtra("details", clickedResponse);
        startActivity(intent);
    }

    @Override
    public void onGetIncidentList(ArrayList<IncidentListResponse> incidentListResponse) {
        if (incidentListResponse != null && incidentListResponse != null) {

            incidentListAdapter.setIncidentListResponse(incidentListResponse);
//            Toast.makeText(this, "Done", Toast.LENGTH_LONG).show();
            Log.e("Response List", incidentListResponse.toString());
        } else {
            Toast.makeText(this, "incident list  null!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, "Exception: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {

        try {
            switch (v.getId()) {
                case R.id.toolbar_imageview:
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void commit(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, baseFragment).addToBackStack(null);
        fragmentTransaction.commit();
    }
}
