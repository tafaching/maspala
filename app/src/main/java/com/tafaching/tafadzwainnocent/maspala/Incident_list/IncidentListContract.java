package com.tafaching.tafadzwainnocent.maspala.Incident_list;


import java.util.ArrayList;

public interface IncidentListContract {
    interface View {

    }

    interface IncidentListView {
        void onGetIncidentList(ArrayList<IncidentListResponse> incidentListResponse);

        void onError(String message);
    }

    interface Presenter {
        void requestIncidentList(IncidentListRequest request, IncidentListView view);
    }
}
