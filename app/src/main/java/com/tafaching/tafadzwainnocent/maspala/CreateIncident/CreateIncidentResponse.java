package com.tafaching.tafadzwainnocent.maspala.CreateIncident;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreateIncidentResponse implements Serializable {

    @SerializedName("Message")
    private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
