package com.tafaching.tafadzwainnocent.maspala.Incident_list;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class IncidentListRequest implements Serializable {
    @SerializedName("mobileNo")
    private String mobileNo;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
