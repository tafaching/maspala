package com.tafaching.tafadzwainnocent.maspala.Gender;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GenderRequest implements Serializable {
    @SerializedName("genders")
    private String genders;

    public String getGenders() {
        return genders;
    }

    public void setGenders(String genders) {
        this.genders = genders;
    }
}
