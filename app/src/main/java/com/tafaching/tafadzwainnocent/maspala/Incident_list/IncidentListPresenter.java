package com.tafaching.tafadzwainnocent.maspala.Incident_list;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tafaching.tafadzwainnocent.maspala.Network.Api;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class IncidentListPresenter implements IncidentListContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static IncidentListPresenter INSTANCE;
    private Api api;

    private IncidentListPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    static IncidentListPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new IncidentListPresenter(context.getApplicationContext());
    }


    @Override
    public void requestIncidentList(IncidentListRequest request, IncidentListContract.IncidentListView view) {
        api.getIncListDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onGetIncidentList(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }
}
