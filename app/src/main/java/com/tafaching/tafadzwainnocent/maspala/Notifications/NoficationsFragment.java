package com.tafaching.tafadzwainnocent.maspala.Notifications;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.Util.Sharepref;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoficationsFragment extends BaseFragment implements NotificationContract.NotificationView, NotificationAdapter.OnItemClick , View.OnClickListener {
    NotificationPresenter presenter;
    private RecyclerView recyclerView;
    NotificationAdapter notificationAdapter;
    private ArrayList<NotificationResponse> notificationResponsess;

    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;

    @Override
    public int getContentView() {
        return R.layout.fragment_nofications;
    }

    @Override
    protected void setLayoutRef(View view, Bundle savedInstanceState) {
        presenter = NotificationPresenter.getInstance(getActivity());
        toolbar_imgClose =view.findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = view.findViewById(R.id.toolbar_tvTitle);
        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar_imgClose.setOnClickListener(this);
        String xyz = "Notifications";
        toolbar_tvTitle.setText(xyz);
        notificationResponsess = new ArrayList<>();
        recyclerView = view.findViewById(R.id.notifications_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        notificationAdapter = new NotificationAdapter(notificationResponsess, getActivity(), this::onClickMetro);
        recyclerView.setAdapter(notificationAdapter);
        post();
    }

    public void post() {

        Integer orgId = Sharepref.getOrganisationId(getActivity());
        NotificationRequest request = new NotificationRequest();
        request.setOrganisationId(orgId);
        presenter.requestNotificationList(request, this);
    }

    @Override
    public void onClickMetro(NotificationResponse clickedResponse) {

    }

    @Override
    public void onGetNotificationList(ArrayList<NotificationResponse> notificationResponsess) {
        if (notificationResponsess != null && notificationResponsess != null) {

            notificationAdapter.setNotificationResponse(notificationResponsess);

            Log.e("Response List", notificationResponsess.toString());
        } else {
            Toast.makeText(getActivity(), "notificationResponses list  null!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(getActivity(), "Exception: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {

        try {
            switch (v.getId()) {
                case R.id.toolbar_imageview:
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
