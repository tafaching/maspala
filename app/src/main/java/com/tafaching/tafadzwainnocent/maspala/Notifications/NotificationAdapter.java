package com.tafaching.tafadzwainnocent.maspala.Notifications;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.tafaching.tafadzwainnocent.maspala.R;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
    private ArrayList<NotificationResponse> notificationResponse;
    private Context context;
    private NotificationAdapter.OnItemClick onItemClick;

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        TextView title,not_date,description;

        View currentView;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            currentView = itemView;
            title = itemView.findViewById(R.id.notification_title);
            not_date = itemView.findViewById(R.id.notification_date);
            description = itemView.findViewById(R.id.notification_description);

        }
    }

    public NotificationAdapter(ArrayList<NotificationResponse> notificationResponse, Context context, NotificationAdapter.OnItemClick onItemClick) {
        this.notificationResponse = notificationResponse;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    public void setNotificationResponse(ArrayList<NotificationResponse> notificationResponse) {
        this.notificationResponse = notificationResponse;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_notifications, viewGroup, false);
        return new NotificationAdapter.NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.NotificationViewHolder holder, int i) {

        holder.title.setText(notificationResponse.get(i).getSubject());
        holder.not_date.setText(notificationResponse.get(i).getPublishDate());
        holder.description.setText(notificationResponse.get(i).getMessage());
        holder.currentView.setOnClickListener(v -> {
            if ((onItemClick != null)) {
                onItemClick.onClickMetro(notificationResponse.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationResponse.size();
    }

    public interface OnItemClick {
        void onClickMetro(final NotificationResponse clickedResponse);
    }
}
