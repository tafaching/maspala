package com.tafaching.tafadzwainnocent.maspala.TrafficFinesLoginReg;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrafficRegFragment extends BaseFragment {


    @Override
    public int getContentView() {
        return R.layout.fragment_traffic_reg;
    }

    @Override
    protected void setLayoutRef(View view, Bundle savedInstanceState) {

    }
}
