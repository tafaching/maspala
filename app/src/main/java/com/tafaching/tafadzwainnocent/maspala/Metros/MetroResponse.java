package com.tafaching.tafadzwainnocent.maspala.Metros;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class MetroResponse implements Serializable {
    @SerializedName("OrganisationId")
    private Integer OrganisationId;
    @SerializedName("OrganisationTypeId")
    private Integer OrganisationTypeId;
    @SerializedName("OrganisationType")
    private String OrganisationType;
    @SerializedName("Name")
    private String Name;
    @SerializedName("MunicipalityCode")
    private String MunicipalityCode;
    @SerializedName("RegistrationNumber")
    private String RegistrationNumber;
    @SerializedName("DistrictId")
    private Integer DistrictId;
    @SerializedName("DistrictName")
    private String DistrictName;
    @SerializedName("PhysicalAddress")
    private String PhysicalAddress;
    @SerializedName("PhoneNumber")
    private String PhoneNumber;
    @SerializedName("FaxNumber")
    private String FaxNumber;
    @SerializedName("Email")
    private String Email;
    @SerializedName("CountryId")
    private Integer CountryId;
    @SerializedName("Country")
    private String Country;
    @SerializedName("ProvinceId")
    private Integer ProvinceId;
    @SerializedName("Province")
    private String Province;
    @SerializedName("RegionId")
    private Integer RegionId;
    @SerializedName("Region")
    private String Region;
    @SerializedName("CityId")
    private Integer CityId;
    @SerializedName("City")
    private String City;
    @SerializedName("About")
    private String About;
    @SerializedName("Mission")
    private String Mission;
    @SerializedName("Vision")
    private String Vision;
    @SerializedName("WebsiteUrl")
    private String WebsiteUrl;
    @SerializedName("BasedUrl")
    private String BasedUrl;
    @SerializedName("Latitude")
    private String Latitude;
    @SerializedName("Longitude")
    private String Longitude;
    @SerializedName("WhatsAppNumber")
    private String WhatsAppNumber;
    @SerializedName("CreatedByUser")
    private String CreatedByUser;
    @SerializedName("ModifiedByUser")
    private String ModifiedByUser;
    @SerializedName("CreatedBy")
    private String CreatedBy;
    @SerializedName("ModifiedBy")
    private String ModifiedBy;
    @SerializedName("DateCreated")
    private String DateCreated;
    @SerializedName("DateModified")
    private String DateModified;
    @SerializedName("Active")
    private Boolean Active;
    @SerializedName("Councillor")
    private String Councillor;
    @SerializedName("Councillors")
    private ArrayList<Councillors> councillors;
    @SerializedName("Bytes")
    private String Bytes;
    @SerializedName("OrganisationLibrary")
    private OrganisationLibrary organisationLibrary;

    private boolean state;

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }

    public Integer getOrganisationTypeId() {
        return OrganisationTypeId;
    }

    public void setOrganisationTypeId(Integer organisationTypeId) {
        OrganisationTypeId = organisationTypeId;
    }

    public String getOrganisationType() {
        return OrganisationType;
    }

    public void setOrganisationType(String organisationType) {
        OrganisationType = organisationType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMunicipalityCode() {
        return MunicipalityCode;
    }

    public void setMunicipalityCode(String municipalityCode) {
        MunicipalityCode = municipalityCode;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        RegistrationNumber = registrationNumber;
    }

    public Integer getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(Integer districtId) {
        DistrictId = districtId;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getPhysicalAddress() {
        return PhysicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        PhysicalAddress = physicalAddress;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return FaxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        FaxNumber = faxNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Integer getCountryId() {
        return CountryId;
    }

    public void setCountryId(Integer countryId) {
        CountryId = countryId;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public Integer getProvinceId() {
        return ProvinceId;
    }

    public void setProvinceId(Integer provinceId) {
        ProvinceId = provinceId;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public Integer getRegionId() {
        return RegionId;
    }

    public void setRegionId(Integer regionId) {
        RegionId = regionId;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAbout() {
        return About;
    }

    public void setAbout(String about) {
        About = about;
    }

    public String getMission() {
        return Mission;
    }

    public void setMission(String mission) {
        Mission = mission;
    }

    public String getVision() {
        return Vision;
    }

    public void setVision(String vision) {
        Vision = vision;
    }

    public String getWebsiteUrl() {
        return WebsiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        WebsiteUrl = websiteUrl;
    }

    public String getBasedUrl() {
        return BasedUrl;
    }

    public void setBasedUrl(String basedUrl) {
        BasedUrl = basedUrl;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getWhatsAppNumber() {
        return WhatsAppNumber;
    }

    public void setWhatsAppNumber(String whatsAppNumber) {
        WhatsAppNumber = whatsAppNumber;
    }

    public String getCreatedByUser() {
        return CreatedByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        CreatedByUser = createdByUser;
    }

    public String getModifiedByUser() {
        return ModifiedByUser;
    }

    public void setModifiedByUser(String modifiedByUser) {
        ModifiedByUser = modifiedByUser;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateModified() {
        return DateModified;
    }

    public void setDateModified(String dateModified) {
        DateModified = dateModified;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public String getCouncillor() {
        return Councillor;
    }

    public void setCouncillor(String councillor) {
        Councillor = councillor;
    }

    public ArrayList<Councillors> getCouncillors() {
        return councillors;
    }

    public void setCouncillors(ArrayList<Councillors> councillors) {
        this.councillors = councillors;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String bytes) {
        Bytes = bytes;
    }

    public OrganisationLibrary getOrganisationLibrary() {
        return organisationLibrary;
    }

    public void setOrganisationLibrary(OrganisationLibrary organisationLibrary) {
        this.organisationLibrary = organisationLibrary;
    }
}
