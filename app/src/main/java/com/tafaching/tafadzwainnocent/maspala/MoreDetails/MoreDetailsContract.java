package com.tafaching.tafadzwainnocent.maspala.MoreDetails;

import com.tafaching.tafadzwainnocent.maspala.Metros.MetroResponse;


public interface MoreDetailsContract {
    interface View {

    }

    interface MoreDetailsView {
        void onGetOrgDetails(MetroResponse metroResponses);

        void onError(String message);
    }

    interface Presenter {
        void requestOrgDetails(MoreDetailsRequest request, MoreDetailsView view);
    }
}
