package com.tafaching.tafadzwainnocent.maspala.Incident_list;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class IncidentListResponse implements Serializable {
    @SerializedName("IncidentId")
    private Integer IncidentId;
    @SerializedName("ReferenceNumber")
    private String ReferenceNumber;
    @SerializedName("OrganisationId")
    private Integer OrganisationId;
    @SerializedName("Organisation")
    private String Organisation;
    @SerializedName("CustomerFirstName")
    private String CustomerFirstName;
    @SerializedName("CustomerSurname")
    private String CustomerSurname;
    @SerializedName("CustomerFullName")
    private String CustomerFullName;
    @SerializedName("CustomerEmail")
    private String CustomerEmail;
    @SerializedName("ContactNo")
    private String ContactNo;
    @SerializedName("GenderId")
    private Integer GenderId;
    @SerializedName("Gender")
    private String Gender;
    @SerializedName("Id_Passport")
    private String Id_Passport;
    @SerializedName("PlotNo")
    private String PlotNo;
    @SerializedName("Address")
    private String Address;
    @SerializedName("SuburbId")
    private Integer SuburbId;
    @SerializedName("Suburb")
    private String Suburb;
    @SerializedName("CityId")
    private Integer CityId;
    @SerializedName("City")
    private String City;
    @SerializedName("CouncilId")
    private Integer CouncilId;
    @SerializedName("Council")
    private String Council;
    @SerializedName("WardId")
    private Integer WardId;
    @SerializedName("Ward")
    private String Ward;
    @SerializedName("Latitude")
    private String Latitude;
    @SerializedName("Longitude")
    private String Longitude;
    @SerializedName("IncidentTypeId")
    private Integer IncidentTypeId;
    @SerializedName("IncidentType")
    private String IncidentType;
    @SerializedName("StatusId")
    private Integer StatusId;
    @SerializedName("Status")
    private String Status;
    @SerializedName("PriorityId")
    private Integer PriorityId;
    @SerializedName("Priority")
    private String Priority;
    @SerializedName("ImpactId")
    private Integer ImpactId;
    @SerializedName("Impact")
    private String Impact;
    @SerializedName("UrgencyId")
    private Integer UrgencyId;
    @SerializedName("Urgency")
    private String Urgency;
    @SerializedName("ReportedMediumId")
    private Integer ReportedMediumId;
    @SerializedName("ReportedMedium")
    private String ReportedMedium;
    @SerializedName("RecoveryTimeId")
    private Integer RecoveryTimeId;
    @SerializedName("RecoveryTime")
    private String RecoveryTime;
    @SerializedName("DateRecieved")
    private String DateRecieved;
    @SerializedName("TimeRecieved")
    private String TimeRecieved;
    @SerializedName("ResolvedDate")
    private String ResolvedDate;
    @SerializedName("DateClosed")
    private String DateClosed;
    @SerializedName("ClosedBy")
    private String ClosedBy;
    @SerializedName("CustomerComment")
    private String CustomerComment;
    @SerializedName("Comment")
    private String Comment;
    @SerializedName("Active")
    private boolean Active;
    @SerializedName("Libraries")
    private ArrayList<Libraries> Libraries;
    @SerializedName("Attachments")
    private String Attachments;
    @SerializedName("FileAttachments")
    private String FileAttachments;
    @SerializedName("Bytes")
    private String Bytes;
    @SerializedName("IncidentTasks")
    private ArrayList<IncidentTasks> IncidentTasks;
    @SerializedName("IncidentTask")
    private String IncidentTask;
    @SerializedName("IncidentSubTasks")
    private ArrayList<IncidentSubTasks> IncidentSubTasks;
    @SerializedName("IncidentSubTask")
    private String IncidentSubTask;
    @SerializedName("IncidentCrews")
    private ArrayList<IncidentCrews> IncidentCrews;
    @SerializedName("IncidentCrew")
    private String IncidentCrew;
    @SerializedName("IncidentComments")
    private ArrayList<IncidentComments> IncidentComments;
    @SerializedName("IncidentComment")
    private String IncidentComment;
    @SerializedName("EscalateIncidents")
    private ArrayList<EscalateIncidents> EscalateIncidents;
    @SerializedName("EscalateIncident")
    private String EscalateIncident;
    @SerializedName("Feedbacks")
    private ArrayList<Feedbacks> Feedbacks;
    @SerializedName("Feedback")
    private String Feedback;
    @SerializedName("DepartmentId")
    private Integer DepartmentId;
    @SerializedName("Department")
    private String Department;
    @SerializedName("BusinessUnitId")
    private Integer BusinessUnitId;
    @SerializedName("BusinessUnit")
    private String BusinessUnit;
    @SerializedName("RoleName")
    private String RoleName;
    @SerializedName("IncidentOwner")
    private String IncidentOwner;
    @SerializedName("OwnerTaskStatus")
    private String OwnerTaskStatus;

    public Integer getIncidentId() {
        return IncidentId;
    }

    public void setIncidentId(Integer incidentId) {
        IncidentId = incidentId;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }

    public String getOrganisation() {
        return Organisation;
    }

    public void setOrganisation(String organisation) {
        Organisation = organisation;
    }

    public String getCustomerFirstName() {
        return CustomerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        CustomerFirstName = customerFirstName;
    }

    public String getCustomerSurname() {
        return CustomerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        CustomerSurname = customerSurname;
    }

    public String getCustomerFullName() {
        return CustomerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        CustomerFullName = customerFullName;
    }

    public String getCustomerEmail() {
        return CustomerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        CustomerEmail = customerEmail;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public Integer getGenderId() {
        return GenderId;
    }

    public void setGenderId(Integer genderId) {
        GenderId = genderId;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getId_Passport() {
        return Id_Passport;
    }

    public void setId_Passport(String id_Passport) {
        Id_Passport = id_Passport;
    }

    public String getPlotNo() {
        return PlotNo;
    }

    public void setPlotNo(String plotNo) {
        PlotNo = plotNo;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public Integer getSuburbId() {
        return SuburbId;
    }

    public void setSuburbId(Integer suburbId) {
        SuburbId = suburbId;
    }

    public String getSuburb() {
        return Suburb;
    }

    public void setSuburb(String suburb) {
        Suburb = suburb;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public Integer getCouncilId() {
        return CouncilId;
    }

    public void setCouncilId(Integer councilId) {
        CouncilId = councilId;
    }

    public String getCouncil() {
        return Council;
    }

    public void setCouncil(String council) {
        Council = council;
    }

    public Integer getWardId() {
        return WardId;
    }

    public void setWardId(Integer wardId) {
        WardId = wardId;
    }

    public String getWard() {
        return Ward;
    }

    public void setWard(String ward) {
        Ward = ward;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public Integer getIncidentTypeId() {
        return IncidentTypeId;
    }

    public void setIncidentTypeId(Integer incidentTypeId) {
        IncidentTypeId = incidentTypeId;
    }

    public String getIncidentType() {
        return IncidentType;
    }

    public void setIncidentType(String incidentType) {
        IncidentType = incidentType;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public void setStatusId(Integer statusId) {
        StatusId = statusId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Integer getPriorityId() {
        return PriorityId;
    }

    public void setPriorityId(Integer priorityId) {
        PriorityId = priorityId;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public Integer getImpactId() {
        return ImpactId;
    }

    public void setImpactId(Integer impactId) {
        ImpactId = impactId;
    }

    public String getImpact() {
        return Impact;
    }

    public void setImpact(String impact) {
        Impact = impact;
    }

    public Integer getUrgencyId() {
        return UrgencyId;
    }

    public void setUrgencyId(Integer urgencyId) {
        UrgencyId = urgencyId;
    }

    public String getUrgency() {
        return Urgency;
    }

    public void setUrgency(String urgency) {
        Urgency = urgency;
    }

    public Integer getReportedMediumId() {
        return ReportedMediumId;
    }

    public void setReportedMediumId(Integer reportedMediumId) {
        ReportedMediumId = reportedMediumId;
    }

    public String getReportedMedium() {
        return ReportedMedium;
    }

    public void setReportedMedium(String reportedMedium) {
        ReportedMedium = reportedMedium;
    }

    public Integer getRecoveryTimeId() {
        return RecoveryTimeId;
    }

    public void setRecoveryTimeId(Integer recoveryTimeId) {
        RecoveryTimeId = recoveryTimeId;
    }

    public String getRecoveryTime() {
        return RecoveryTime;
    }

    public void setRecoveryTime(String recoveryTime) {
        RecoveryTime = recoveryTime;
    }

    public String getDateRecieved() {
        return DateRecieved;
    }

    public void setDateRecieved(String dateRecieved) {
        DateRecieved = dateRecieved;
    }

    public String getTimeRecieved() {
        return TimeRecieved;
    }

    public void setTimeRecieved(String timeRecieved) {
        TimeRecieved = timeRecieved;
    }

    public String getResolvedDate() {
        return ResolvedDate;
    }

    public void setResolvedDate(String resolvedDate) {
        ResolvedDate = resolvedDate;
    }

    public String getDateClosed() {
        return DateClosed;
    }

    public void setDateClosed(String dateClosed) {
        DateClosed = dateClosed;
    }

    public String getClosedBy() {
        return ClosedBy;
    }

    public void setClosedBy(String closedBy) {
        ClosedBy = closedBy;
    }

    public String getCustomerComment() {
        return CustomerComment;
    }

    public void setCustomerComment(String customerComment) {
        CustomerComment = customerComment;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.Libraries> getLibraries() {
        return Libraries;
    }

    public void setLibraries(ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.Libraries> libraries) {
        Libraries = libraries;
    }

    public String getAttachments() {
        return Attachments;
    }

    public void setAttachments(String attachments) {
        Attachments = attachments;
    }

    public String getFileAttachments() {
        return FileAttachments;
    }

    public void setFileAttachments(String fileAttachments) {
        FileAttachments = fileAttachments;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String bytes) {
        Bytes = bytes;
    }

    public ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentTasks> getIncidentTasks() {
        return IncidentTasks;
    }

    public void setIncidentTasks(ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentTasks> incidentTasks) {
        IncidentTasks = incidentTasks;
    }

    public String getIncidentTask() {
        return IncidentTask;
    }

    public void setIncidentTask(String incidentTask) {
        IncidentTask = incidentTask;
    }

    public ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentSubTasks> getIncidentSubTasks() {
        return IncidentSubTasks;
    }

    public void setIncidentSubTasks(ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentSubTasks> incidentSubTasks) {
        IncidentSubTasks = incidentSubTasks;
    }

    public String getIncidentSubTask() {
        return IncidentSubTask;
    }

    public void setIncidentSubTask(String incidentSubTask) {
        IncidentSubTask = incidentSubTask;
    }

    public ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentCrews> getIncidentCrews() {
        return IncidentCrews;
    }

    public void setIncidentCrews(ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentCrews> incidentCrews) {
        IncidentCrews = incidentCrews;
    }

    public String getIncidentCrew() {
        return IncidentCrew;
    }

    public void setIncidentCrew(String incidentCrew) {
        IncidentCrew = incidentCrew;
    }

    public ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentComments> getIncidentComments() {
        return IncidentComments;
    }

    public void setIncidentComments(ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentComments> incidentComments) {
        IncidentComments = incidentComments;
    }

    public String getIncidentComment() {
        return IncidentComment;
    }

    public void setIncidentComment(String incidentComment) {
        IncidentComment = incidentComment;
    }

    public ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.EscalateIncidents> getEscalateIncidents() {
        return EscalateIncidents;
    }

    public void setEscalateIncidents(ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.EscalateIncidents> escalateIncidents) {
        EscalateIncidents = escalateIncidents;
    }

    public String getEscalateIncident() {
        return EscalateIncident;
    }

    public void setEscalateIncident(String escalateIncident) {
        EscalateIncident = escalateIncident;
    }

    public ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.Feedbacks> getFeedbacks() {
        return Feedbacks;
    }

    public void setFeedbacks(ArrayList<com.tafaching.tafadzwainnocent.maspala.Incident_list.Feedbacks> feedbacks) {
        Feedbacks = feedbacks;
    }

    public String getFeedback() {
        return Feedback;
    }

    public void setFeedback(String feedback) {
        Feedback = feedback;
    }

    public Integer getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        DepartmentId = departmentId;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public Integer getBusinessUnitId() {
        return BusinessUnitId;
    }

    public void setBusinessUnitId(Integer businessUnitId) {
        BusinessUnitId = businessUnitId;
    }

    public String getBusinessUnit() {
        return BusinessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        BusinessUnit = businessUnit;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }

    public String getIncidentOwner() {
        return IncidentOwner;
    }

    public void setIncidentOwner(String incidentOwner) {
        IncidentOwner = incidentOwner;
    }

    public String getOwnerTaskStatus() {
        return OwnerTaskStatus;
    }

    public void setOwnerTaskStatus(String ownerTaskStatus) {
        OwnerTaskStatus = ownerTaskStatus;
    }
}
