//package com.tafaching.tafadzwainnocent.maspala.Metros;
//
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//import android.widget.Toast;
//
//import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
//import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
//import com.tafaching.tafadzwainnocent.maspala.R;
//
//import java.util.ArrayList;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class LocalFragment extends BaseFragment implements MetroContract.MetroView, LocalAdapter.OnItemClick {
//    MetroPresenter presenter;
//
//    private RecyclerView recyclerView;
//    LocalAdapter metroAdapter;
//    private ArrayList<MetroResponse> metroResponses;
//
//    @Override
//    public int getContentView() {
//        return R.layout.fragment_metro;
//    }
//
//    @Override
//    protected void setLayoutRef(View view, Bundle savedInstanceState) {
//        presenter = MetroPresenter.getInstance(getActivity());
//        metroResponses = new ArrayList<>();
//        recyclerView = (RecyclerView) view.findViewById(R.id.my_metro);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        metroAdapter = new LocalAdapter(metroResponses, getActivity(), this::onClickMetro);
//        recyclerView.setAdapter(metroAdapter);
//    post();
//    }
//    public void post() {
//        String org ="getorganisations";
//        MetroRequest request = new MetroRequest();
//        request.setGetorganisations(org);
//        presenter.requestOrg(request,this);
//    }
//
//    @Override
//    public void onClickMetro(MetroResponse clickedResponse) {
//        Intent intent = new Intent(getActivity(), DashboardActivity.class);
//        startActivity(intent);
//    }
//
//    @Override
//    public void onGetOrg(ArrayList<MetroResponse> metroResponses) {
//
//        if (metroResponses != null && metroResponses != null) {
//            metroAdapter.setMetroResponse(metroResponses);
//
//
//        }
//    }
//
//    @Override
//    public void onError(String message) {
//        Toast.makeText(getActivity(), "Exception: " + message, Toast.LENGTH_LONG).show();
//
//    }
//}
