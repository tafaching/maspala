package com.tafaching.tafadzwainnocent.maspala.TenderList;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tafaching.tafadzwainnocent.maspala.Network.Api;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TenderPresenter implements TenderContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static TenderPresenter INSTANCE;
    private Api api;

    private TenderPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    static TenderPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new TenderPresenter(context.getApplicationContext());
    }



    @Override
    public void requestTenderList(TenderRequest request, TenderContract.TenderView view) {
        api.getTenderDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onGetTenderList(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }
}
