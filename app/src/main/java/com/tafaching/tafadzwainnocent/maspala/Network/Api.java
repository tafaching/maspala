package com.tafaching.tafadzwainnocent.maspala.Network;




import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesRequest;
import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesResponse;
import com.tafaching.tafadzwainnocent.maspala.CreateIncident.CreateIncidentRequest;
import com.tafaching.tafadzwainnocent.maspala.CreateIncident.CreateIncidentResponse;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderRequest;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderResponse;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesRequest;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesResponse;
import com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentListRequest;
import com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentListResponse;
import com.tafaching.tafadzwainnocent.maspala.Metros.MetroRequest;
import com.tafaching.tafadzwainnocent.maspala.Metros.MetroResponse;
import com.tafaching.tafadzwainnocent.maspala.MoreDetails.MoreDetailsRequest;
import com.tafaching.tafadzwainnocent.maspala.Notifications.NotificationRequest;
import com.tafaching.tafadzwainnocent.maspala.Notifications.NotificationResponse;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbRequest;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbResponse;
import com.tafaching.tafadzwainnocent.maspala.TenderList.TenderRequest;
import com.tafaching.tafadzwainnocent.maspala.TenderList.TenderResponse;
import com.tafaching.tafadzwainnocent.maspala.Wards.WardRequest;
import com.tafaching.tafadzwainnocent.maspala.Wards.WardResponse;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    public static final String BASE_URL = "http://154.0.165.41:1970/restapi/";
    private static Api instance;
    private final Service service;

    private Api() {
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
        service = retrofit.create(Service.class);
    }

    public static Api start() {
        return instance = instance == null ? new Api() : instance;
    }

    public Observable<ArrayList<MetroResponse>> getOrg(MetroRequest request) {
        return service.getOrganisations(request.getGetorganisations());

    }

    public Observable<MetroResponse> getOrgDetails(MoreDetailsRequest request) {
        return service.getOrgDetails(request.getOrganisationId());

    }
    public Observable<ArrayList<IncidentListResponse>> getIncListDetails(IncidentListRequest request) {
        return service.getIncList(request.getMobileNo());
    }

    public Observable<ArrayList<NotificationResponse>> getNotificationDetails(NotificationRequest request) {
        return service.getNotificationList(request.getOrganisationId());
    }

    public Observable<ArrayList<WardResponse>> getWardDetails(WardRequest request) {
        return service.getWardList(request.getOrganisationId());
    }

    public Observable<ArrayList<TenderResponse>> getTenderDetails(TenderRequest request) {
        return service.getTenderList(request.getOrganisationId());
    }

    public Observable<Integer > createIncident(CreateIncidentRequest request) {
        return service.createIncident(request);
    }

    public Observable<ArrayList<GenderResponse>> getGender(GenderRequest request) {
        return service.getGenderList(request.getGenders());
    }
    public Observable<ArrayList<SuburbResponse>> getSuburb(SuburbRequest request) {
        return service.getSuburbList(request.getSuburbs());
    }

    public Observable<ArrayList<CitiesResponse>> getCities(CitiesRequest request) {
        return service.getCitiesList(request.getCities());
    }

    public Observable<ArrayList<TypesResponse>> getType(TypesRequest request) {
        return service.getTypeList(request.getIncidenttypes());
    }
}
