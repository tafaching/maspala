package com.tafaching.tafadzwainnocent.maspala.CreateIncident;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesRequest;
import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesResponse;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderRequest;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderResponse;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesRequest;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesResponse;
import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbRequest;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbResponse;
import com.tafaching.tafadzwainnocent.maspala.Util.Sharepref;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncidentDetailsFragment extends BaseFragment implements View.OnClickListener, CreateIncidentContract.CreateIncidentView {
    EditText incident_location, street_name, comments;
    TextView back;
    Spinner city, sp_suburb, type;
    Button submit;
    ImageView photo;
    private ArrayList<SuburbResponse> suburbResponseArrayList;
    private CreateIncidentPresenter presenter;

    @Override
    public int getContentView() {
        return R.layout.fragment_incident_details;
    }

    @Override
    protected void setLayoutRef(View view, Bundle savedInstanceState) {
        presenter = CreateIncidentPresenter.getInstance(getActivity());
        suburbResponseArrayList = new ArrayList<>();

        incident_location = view.findViewById(R.id.location);
        street_name = view.findViewById(R.id.street_name);
        city = view.findViewById(R.id.city_town);
        sp_suburb = view.findViewById(R.id.suburb);
        type = view.findViewById(R.id.incident_type);
        comments = view.findViewById(R.id.comments);
        photo = view.findViewById(R.id.take_photo);
        back = view.findViewById(R.id.back);
        back.setOnClickListener(this);
        submit = view.findViewById(R.id.submit);
        submit.setOnClickListener(this);
        postSuburb();
        postCity();
        postType();

    }

    public void postSuburb() {

        String org = "suburbs";
        SuburbRequest request = new SuburbRequest();
        request.setSuburbs(org);
        presenter.selectSuburb(request, this);
    }

    public void postCity() {

        String org = "cities";
        CitiesRequest request = new CitiesRequest();
        request.setCities(org);
        presenter.selectCity(request, this);
    }

    public void postType() {

        String org = "incidenttypes";
        TypesRequest request = new TypesRequest();
        request.setIncidenttypes(org);
        presenter.selectType(request, this);
    }

    public void createIncident() {
        CreateIncidentRequest request = new CreateIncidentRequest();


        String username = Sharepref.getName(getActivity());
        String useraddress = Sharepref.getAddress(getActivity());
        String usersurname = Sharepref.getSurname(getActivity());
        String userid = Sharepref.getIdnumber(getActivity());
        String userphone = Sharepref.getPhoneNumber(getActivity());
        String usergender ="Male";
        Integer orgId = Sharepref.getOrganisationId(getActivity());
        String lat = "-14.282812";
        String lon = "46.27364";
        String cons = "Ward";
        String email = "test@gvt.co.za";
        Integer GenderId = 1;
        Integer SuburbId = 1;
        Integer CityId = 1;
        Integer IncidentTypeId = 1;


        //personal details
        request.setOrganisationId(orgId);
        request.setCustomerFirstName(username);
        request.setCustomerSurname(usersurname);
        request.setCustomerEmail(email);
        request.setCustomerComment(comments.getText().toString().trim());
        request.setContactNo(userphone);
        request.setGenderId(GenderId);
        request.setGender(usergender);
        request.setId_Passport(userid);
        request.setPlotNo(street_name.getText().toString().trim());
        request.setAddress(useraddress);
        request.setSuburbId(SuburbId);
        request.setCityId(CityId);
        request.setIncidentTypeId(IncidentTypeId);


//incident details
        request.setLatitude(lat);
        request.setLongitude(lon);
        request.setCouncil(cons);
        request.setAddress(incident_location.getText().toString().trim());

        request.setCity(city.toString().trim());
        request.setSuburb(sp_suburb.toString().trim());
        request.setIncidentType(type.toString().trim());

//        request.setCity("Soshanguve");
//        request.setSuburb("Lyttelton");
//        request.setIncidentType("Burst Pipe");
        presenter.createIncident(request, this);

    }


    @Override
    public void onCreateIncident(Integer integer) {
        Intent intent = new Intent(getActivity(), DashboardActivity.class);
        startActivity(intent);
        Toast.makeText(getActivity(), "incident submitted!!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSelectSuburb(ArrayList<SuburbResponse> response) {
        suburbSpinner(response);
    }

    @Override
    public void onSelectCity(ArrayList<CitiesResponse> response) {
        citiesSpinner(response);
    }

    @Override
    public void onSelectType(ArrayList<TypesResponse> response) {
        typeSpinner(response);
    }

    private void typeSpinner(ArrayList<TypesResponse> response) {

        String[] items = new String[response.size()];
        for (int i = 0; i < response.size(); i++) {

            items[i] = String.valueOf(response.get(i).getIncidentTypeId());
            items[i] = response.get(i).getDescription();
        }
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);
        type.setAdapter(adapter);

    }


    private void citiesSpinner(ArrayList<CitiesResponse> response) {

        String[] items = new String[response.size()];
        for (int i = 0; i < response.size(); i++) {

            items[i] = String.valueOf(response.get(i).getCityId());
            items[i] = response.get(i).getDescription();
        }
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);
        city.setAdapter(adapter);

    }

    private void suburbSpinner(ArrayList<SuburbResponse> response) {

        String[] items = new String[response.size()];
        for (int i = 0; i < response.size(); i++) {


            items[i] = String.valueOf(response.get(i).getSuburbId());
            items[i] = response.get(i).getDescription();
        }
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);
        sp_suburb.setAdapter(adapter);

    }


    @Override
    public void onError(String message) {
        Toast.makeText(getActivity(), "Exception: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.back:
                parentActivityObserver.commit(new PersonalDetailsFragment());
                break;
            case R.id.submit:

                //Test();
               createIncident();
                break;


            default:
                break;
        }

    }

 public void Test() {
        CreateIncidentRequest request = new CreateIncidentRequest();
        Integer IncidentId = 12;
        Integer OrganisationId = 4;
        String ReferenceNumber = "453543";
        String CustomerFirstName = "Monica";
        String CustomerSurname = "MATANGA";
        String CustomerEmail = "tafaching@gmail.com";
        String CustomerComment = "No Internet connection";
        String ContactNo = "0738837905";
        Integer GenderId = 1;
        String Gender = "Male";
        String Id_Passport = "BNN040995";
        String PlotNo = "714 SANDTON";
        String Address = "714 SANDTON";
        Integer SuburbId = 1;
        String Suburb = "SANDTON";
        Integer CityId = 1;
        String City = "SANDTON";
        Integer CouncilId = 1;
        String Council = "Ward";
        String Latitude = "-14.282812";
        String Longitude = "46.27364";
        Integer IncidentTypeId = 1;
        String IncidentType = "WATER";
        Integer ReportedMediumId = 1;
        String ReportedMedium = "1";

        request.setIncidentId(IncidentId);
        request.setOrganisationId(OrganisationId);
        request.setReferenceNumber(ReferenceNumber);
        request.setCustomerFirstName(CustomerFirstName);
        request.setCustomerSurname(CustomerSurname);
        request.setCustomerEmail(CustomerEmail);
        request.setCustomerComment(CustomerComment);
        request.setContactNo(ContactNo);
        request.setGenderId(GenderId);
        request.setGender(Gender);
        request.setId_Passport(Id_Passport);
        request.setPlotNo(PlotNo);
        request.setAddress(Address);
        request.setSuburbId(SuburbId);
        request.setSuburb(Suburb);
        request.setCityId(CityId);
        request.setCity(City);
        request.setCouncilId(CouncilId);
        request.setCouncil(Council);
        request.setLatitude(Latitude);
        request.setLongitude(Longitude);
        request.setIncidentTypeId(IncidentTypeId);
        request.setIncidentType(IncidentType);
        request.setReportedMediumId(ReportedMediumId);
        request.setReportedMedium(ReportedMedium);

        presenter.createIncident(request, this);
    }

}
