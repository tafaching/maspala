package com.tafaching.tafadzwainnocent.maspala.Dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.R;

public class DashboardActivity extends AppCompatActivity implements  BaseFragment.ParentActivityObserver, View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        commit(new DashboardFragment());

    }

    @Override
    public void commit(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, baseFragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
