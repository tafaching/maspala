package com.tafaching.tafadzwainnocent.maspala.Gender;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tafaching.tafadzwainnocent.maspala.Network.Api;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GenderPresenter implements GenderContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static GenderPresenter INSTANCE;
    private Api api;

    private GenderPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    public static GenderPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new GenderPresenter(context.getApplicationContext());
    }


    @Override
    public void selectGender(GenderRequest request, GenderContract.CenderView view) {
        api.getGender(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onSelectGender(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }
}
