package com.tafaching.tafadzwainnocent.maspala.Suburbs;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SuburbRequest implements Serializable {
    @SerializedName("suburbs")
    private String suburbs;

    public String getSuburbs() {
        return suburbs;
    }

    public void setSuburbs(String suburbs) {
        this.suburbs = suburbs;
    }
}
