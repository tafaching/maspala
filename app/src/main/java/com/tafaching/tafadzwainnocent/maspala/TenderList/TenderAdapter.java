package com.tafaching.tafadzwainnocent.maspala.TenderList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tafaching.tafadzwainnocent.maspala.R;


import java.util.ArrayList;

public class TenderAdapter extends RecyclerView.Adapter<TenderAdapter.TenderViewHolder> {
    private ArrayList<TenderResponse> tenderResponse;
    private Context context;
    private TenderAdapter.OnItemClick onItemClick;

    public class TenderViewHolder extends RecyclerView.ViewHolder {
        TextView tender_id,description,closing;

        View currentView;

        public TenderViewHolder(@NonNull View itemView) {
            super(itemView);
            currentView = itemView;
            tender_id = itemView.findViewById(R.id.tender_id);
            description = itemView.findViewById(R.id.tender_description);
            closing = itemView.findViewById(R.id.tender_closing);


        }
    }

    public TenderAdapter(ArrayList<TenderResponse> tenderResponse, Context context, TenderAdapter.OnItemClick onItemClick) {
        this.tenderResponse = tenderResponse;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    public void setTenderResponse(ArrayList<TenderResponse> tenderResponse) {
        this.tenderResponse = tenderResponse;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TenderAdapter.TenderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_tender, viewGroup, false);
        return new TenderAdapter.TenderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TenderAdapter.TenderViewHolder holder, int i) {

        holder.tender_id.setText(String.valueOf(tenderResponse.get(i).getTenderNo()));
        holder.description.setText(tenderResponse.get(i).getShortDescription());
        holder.closing.setText(tenderResponse.get(i).getClosingDate());
        holder.currentView.setOnClickListener(v -> {
            if ((onItemClick != null)) {
                onItemClick.onClickMetro(tenderResponse.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return tenderResponse.size();
    }

    public interface OnItemClick {
        void onClickMetro(final TenderResponse clickedResponse);
    }
}
