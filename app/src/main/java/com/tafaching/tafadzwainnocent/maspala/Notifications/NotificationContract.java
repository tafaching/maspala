package com.tafaching.tafadzwainnocent.maspala.Notifications;


import java.util.ArrayList;

public interface NotificationContract {
    interface View {

    }

    interface NotificationView {
        void onGetNotificationList(ArrayList<NotificationResponse> notificationResponses);

        void onError(String message);
    }

    interface Presenter {
        void requestNotificationList(NotificationRequest request, NotificationView view);
    }
}
