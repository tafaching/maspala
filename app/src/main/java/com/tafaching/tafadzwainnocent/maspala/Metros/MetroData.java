package com.tafaching.tafadzwainnocent.maspala.Metros;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MetroData implements Serializable {

    @SerializedName("data")
    private ArrayList<MetroResponse> data;

    public ArrayList<MetroResponse> getData() {
        return data;
    }

    public void setData(ArrayList<MetroResponse> data) {
        this.data = data;
    }
}
