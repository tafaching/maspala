package com.tafaching.tafadzwainnocent.maspala.CreateIncident;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesRequest;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesRequest;
import com.tafaching.tafadzwainnocent.maspala.Network.Api;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbRequest;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CreateIncidentPresenter implements CreateIncidentContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static CreateIncidentPresenter INSTANCE;
    private Api api;

    private CreateIncidentPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    public static CreateIncidentPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new CreateIncidentPresenter(context.getApplicationContext());
    }

    @Override
    public void createIncident(CreateIncidentRequest request, CreateIncidentContract.CreateIncidentView view) {
        api.createIncident(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onCreateIncident(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }

    @Override
    public void selectSuburb(SuburbRequest request, CreateIncidentContract.CreateIncidentView view) {
        api.getSuburb(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onSelectSuburb(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }

    @Override
    public void selectCity(CitiesRequest request, CreateIncidentContract.CreateIncidentView view) {
        api.getCities(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onSelectCity(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }

    @Override
    public void selectType(TypesRequest request, CreateIncidentContract.CreateIncidentView view) {
        api.getType(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onSelectType(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }


}
