package com.tafaching.tafadzwainnocent.maspala.CreateIncident;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderContract;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderPresenter;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderRequest;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderResponse;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.Util.Sharepref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.R.layout.simple_spinner_item;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalDetailsFragment extends BaseFragment implements View.OnClickListener, GenderContract.CenderView {
    GenderPresenter presenter;
    private EditText name, surname, idnumber, phone, address;
    Spinner gender;
    TextView back, next;
    private ArrayList<GenderResponse> genderResponsesArrayList;



    @Override
    public int getContentView() {
        return R.layout.fragment_personal_details;
    }

    @Override
    protected void setLayoutRef(View view, Bundle savedInstanceState) {
        presenter = GenderPresenter.getInstance(getActivity());
        genderResponsesArrayList = new ArrayList<>();
        name = view.findViewById(R.id.name);
        surname = view.findViewById(R.id.surname);
        idnumber = view.findViewById(R.id.id_number);
        phone = view.findViewById(R.id.tel_number);
        address = view.findViewById(R.id.address);
        back = view.findViewById(R.id.back);
        next = view.findViewById(R.id.next);
        next.setOnClickListener(this);
        back.setOnClickListener(this);
        gender = view.findViewById(R.id.gender);

        post();
        setDetails();
    }


    public void saveDetails() {
        Sharepref.savePhoneNumber(getContext(), phone.getText().toString().trim());
        Sharepref.saveName(getContext(), name.getText().toString().trim());
        Sharepref.saveSurname(getContext(), surname.getText().toString().trim());
        Sharepref.saveIdnumber(getContext(), idnumber.getText().toString().trim());
        Sharepref.saveName(getContext(), name.getText().toString().trim());
        //Sharepref.saveGender(getContext(), gender.toString().trim());
        Sharepref.saveAddress(getContext(), address.getText().toString().trim());

    }

    public void post() {

        String org = "genders";
        GenderRequest request = new GenderRequest();
        request.setGenders(org);
        presenter.selectGender(request, this);
    }

    public void setDetails() {
        String username = Sharepref.getName(getActivity());
        name.setText(username);

        String useraddress = Sharepref.getAddress(getActivity());
        address.setText(useraddress);

        String usersurname = Sharepref.getSurname(getActivity());
        surname.setText(usersurname);

        String userid = Sharepref.getIdnumber(getActivity());
        idnumber.setText(userid);

        String userphone = Sharepref.getPhoneNumber(getActivity());
        phone.setText(userphone);

        String usergender = Sharepref.getGender(getActivity());

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.back:
                    Intent intents = new Intent(getActivity(), MainActivity.class);
                    startActivity(intents);
                    break;
                case R.id.next:
                    saveDetails();
                    parentActivityObserver.commit(new IncidentDetailsFragment());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSelectGender(ArrayList<GenderResponse> response) {

        showListinSpinner(response);


    }

    //Our method to show list
    private void showListinSpinner(ArrayList<GenderResponse> response) {
        //String array to store all the genders
        String[] items = new String[response.size()];

        //Traversing through the whole list to get all the names
        for (int i = 0; i < response.size(); i++) {
            //Storing names to string array

            items[i]= String.valueOf(response.get(i).getGenderId());
            items[i] = response.get(i).getDescription();
        }

        //Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);
        //setting adapter to spinner
        gender.setAdapter(adapter);
        //Creating an array adapter for list view

    }


    @Override
    public void onError(String message) {
        Toast.makeText(getActivity(), "Exception: " + message, Toast.LENGTH_LONG).show();
    }
}
