package com.tafaching.tafadzwainnocent.maspala.TrafficInfringement;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tafaching.tafadzwainnocent.maspala.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrafficInfringeListFragment extends Fragment {


    public TrafficInfringeListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_traffic_infringe_list, container, false);
    }

}
