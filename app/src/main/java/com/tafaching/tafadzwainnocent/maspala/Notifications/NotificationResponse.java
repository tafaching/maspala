package com.tafaching.tafadzwainnocent.maspala.Notifications;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationResponse implements Serializable {

    @SerializedName("PublicationId")
    private Integer  PublicationId;
    @SerializedName("PublishDate")
    private String  PublishDate;
    @SerializedName("EndDate")
    private String  EndDate;
    @SerializedName("AuthorId")
    private Integer  AuthorId;
    @SerializedName("Author")
    private String  Author;
    @SerializedName("Subject")
    private String  Subject;
    @SerializedName("Message")
    private String  Message;
    @SerializedName("OrganisationId")
    private Integer  OrganisationId;
    @SerializedName("Organisation")
    private String  Organisation;
    @SerializedName("CreatedBy")
    private String  CreatedBy;
    @SerializedName("ModifiedBy")
    private String  ModifiedBy;
    @SerializedName("DateCreated")
    private String  DateCreated;
    @SerializedName("DateModified")
    private String  DateModified;
    @SerializedName("Active")
    private Boolean  Active;
    @SerializedName("CreatedByUser")
    private String  CreatedByUser;
    @SerializedName("ModifiedByUser")
    private String  ModifiedByUser;

    public Integer getPublicationId() {
        return PublicationId;
    }

    public void setPublicationId(Integer publicationId) {
        PublicationId = publicationId;
    }

    public String getPublishDate() {
        return PublishDate;
    }

    public void setPublishDate(String publishDate) {
        PublishDate = publishDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public Integer getAuthorId() {
        return AuthorId;
    }

    public void setAuthorId(Integer authorId) {
        AuthorId = authorId;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }

    public String getOrganisation() {
        return Organisation;
    }

    public void setOrganisation(String organisation) {
        Organisation = organisation;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateModified() {
        return DateModified;
    }

    public void setDateModified(String dateModified) {
        DateModified = dateModified;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public String getCreatedByUser() {
        return CreatedByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        CreatedByUser = createdByUser;
    }

    public String getModifiedByUser() {
        return ModifiedByUser;
    }

    public void setModifiedByUser(String modifiedByUser) {
        ModifiedByUser = modifiedByUser;
    }
}
