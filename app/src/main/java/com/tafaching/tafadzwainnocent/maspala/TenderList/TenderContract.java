package com.tafaching.tafadzwainnocent.maspala.TenderList;


import java.util.ArrayList;

public interface TenderContract {
    interface View {

    }

    interface TenderView {
        void onGetTenderList(ArrayList<TenderResponse> tenderResponses);

        void onError(String message);
    }

    interface Presenter {
        void requestTenderList(TenderRequest request, TenderView view);
    }
}
