package com.tafaching.tafadzwainnocent.maspala.TrafficFinesLoginReg;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrafficLognFragment extends BaseFragment implements View.OnClickListener {
TextView register_txt;
Button sign_in;
    @Override
    public int getContentView() {
        return R.layout.fragment_traffic_logn;
    }

    @Override
    protected void setLayoutRef(View view, Bundle savedInstanceState) {
        sign_in=view.findViewById(R.id.sign_in);
        sign_in.setOnClickListener(this);
        register_txt=view.findViewById(R.id.register_now);
        register_txt.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.register_now:
                    parentActivityObserver.commit(new TrafficRegFragment());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
