package com.tafaching.tafadzwainnocent.maspala.TenderList;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.TenderDetails.TenderDetailsActivity;
import com.tafaching.tafadzwainnocent.maspala.Util.Sharepref;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenderFragment extends BaseFragment implements TenderContract.TenderView, TenderAdapter.OnItemClick, View.OnClickListener {
    TenderPresenter presenter;
    private RecyclerView recyclerView;
    TenderAdapter tenderAdapter;
    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;

    private ArrayList<TenderResponse> tenderResponses;

    @Override
    public int getContentView() {
        return R.layout.fragment_tender;
    }

    @Override
    protected void setLayoutRef(View view, Bundle savedInstanceState) {
        presenter = TenderPresenter.getInstance(getActivity());
        tenderResponses = new ArrayList<>();
        recyclerView = view.findViewById(R.id.tender_view);
        toolbar_imgClose =view.findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = view.findViewById(R.id.toolbar_tvTitle);
        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar_imgClose.setOnClickListener(this);
        String xyz = "Tender List";
        toolbar_tvTitle.setText(xyz);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        tenderAdapter = new TenderAdapter(tenderResponses, getActivity(), this::onClickMetro);
        recyclerView.setAdapter(tenderAdapter);
        post();
    }

    public void post() {
        Integer orgId = Sharepref.getOrganisationId(getActivity());
        TenderRequest request = new TenderRequest();
        request.setOrganisationId(orgId);
        presenter.requestTenderList(request, this);
    }

    @Override
    public void onClickMetro(TenderResponse clickedResponse) {

        Intent intent = new Intent(getActivity(), TenderDetailsActivity.class);
        intent.putExtra("details", clickedResponse);
        startActivity(intent);
    }

    @Override
    public void onGetTenderList(ArrayList<TenderResponse> tenderResponses) {
        if (tenderResponses != null) {

            tenderAdapter.setTenderResponse(tenderResponses);

            Log.e("Response List", tenderResponses.toString());
        } else {
            Toast.makeText(getActivity(), "tenderResponses list  null!!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onError(String message) {
        Toast.makeText(getActivity(), "Exception: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.toolbar_imageview:
                    Intent intent = new Intent(getActivity(), DashboardActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
