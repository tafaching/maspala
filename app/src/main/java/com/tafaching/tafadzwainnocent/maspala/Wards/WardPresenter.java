package com.tafaching.tafadzwainnocent.maspala.Wards;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tafaching.tafadzwainnocent.maspala.Network.Api;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class WardPresenter implements WardContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static WardPresenter INSTANCE;
    private Api api;

    private WardPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    static WardPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new WardPresenter(context.getApplicationContext());
    }


    @Override
    public void requestWardList(WardRequest request, WardContract.WardView view) {
        api.getWardDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onGetWardList(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }
}
