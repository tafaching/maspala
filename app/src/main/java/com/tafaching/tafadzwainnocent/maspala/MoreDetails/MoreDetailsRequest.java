package com.tafaching.tafadzwainnocent.maspala.MoreDetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MoreDetailsRequest implements Serializable {

    @SerializedName("OrganisationId")
    private Integer OrganisationId;

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }
}
