package com.tafaching.tafadzwainnocent.maspala.Wards;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CouncillorLibrary implements Serializable {
    @SerializedName("CouncillorLibraryId")
    private Integer CouncillorLibraryId;
    @SerializedName("CouncillorId")
    private Integer CouncillorId;
    @SerializedName("FileName")
    private String FileName;
    @SerializedName("FileSize")
    private String FileSize;
    @SerializedName("FileType")
    private String FileType;
    @SerializedName("FilePath")
    private String FilePath;
    @SerializedName("CreatedByUser")
    private String CreatedByUser;
    @SerializedName("ModifiedByUser")
    private String ModifiedByUser;
    @SerializedName("CreatedBy")
    private String CreatedBy;
    @SerializedName("ModifiedBy")
    private String ModifiedBy;
    @SerializedName("DateCreated")
    private String DateCreated;
    @SerializedName("DateModified")
    private String DateModified;
    @SerializedName("Active")
    private Boolean Active;

    public Integer getCouncillorLibraryId() {
        return CouncillorLibraryId;
    }

    public void setCouncillorLibraryId(Integer councillorLibraryId) {
        CouncillorLibraryId = councillorLibraryId;
    }

    public Integer getCouncillorId() {
        return CouncillorId;
    }

    public void setCouncillorId(Integer councillorId) {
        CouncillorId = councillorId;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileSize() {
        return FileSize;
    }

    public void setFileSize(String fileSize) {
        FileSize = fileSize;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }

    public String getCreatedByUser() {
        return CreatedByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        CreatedByUser = createdByUser;
    }

    public String getModifiedByUser() {
        return ModifiedByUser;
    }

    public void setModifiedByUser(String modifiedByUser) {
        ModifiedByUser = modifiedByUser;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateModified() {
        return DateModified;
    }

    public void setDateModified(String dateModified) {
        DateModified = dateModified;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }
}
