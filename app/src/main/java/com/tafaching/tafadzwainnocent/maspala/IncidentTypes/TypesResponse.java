package com.tafaching.tafadzwainnocent.maspala.IncidentTypes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TypesResponse implements Serializable {

    @SerializedName("IncidentTypeId")
    private Integer IncidentTypeId;
    @SerializedName("OrganisationId")
    private Integer OrganisationId;
    @SerializedName("Organisation")
    private String Organisation;
    @SerializedName("Description")
    private String Description;
    @SerializedName("DepartmentId")
    private Integer DepartmentId;
    @SerializedName("Department")
    private String Department;
    @SerializedName("CreatedByUser")
    private String CreatedByUser;
    @SerializedName("ModifiedByUser")
    private String ModifiedByUser;
    @SerializedName("CreatedBy")
    private String CreatedBy;
    @SerializedName("ModifiedBy")
    private String ModifiedBy;
    @SerializedName("DateCreated")
    private String DateCreated;
    @SerializedName("DateModified")
    private String DateModified;
    @SerializedName("Active")
    private String Active;

    public Integer getIncidentTypeId() {
        return IncidentTypeId;
    }

    public void setIncidentTypeId(Integer incidentTypeId) {
        IncidentTypeId = incidentTypeId;
    }

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }

    public String getOrganisation() {
        return Organisation;
    }

    public void setOrganisation(String organisation) {
        Organisation = organisation;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Integer getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        DepartmentId = departmentId;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getCreatedByUser() {
        return CreatedByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        CreatedByUser = createdByUser;
    }

    public String getModifiedByUser() {
        return ModifiedByUser;
    }

    public void setModifiedByUser(String modifiedByUser) {
        ModifiedByUser = modifiedByUser;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateModified() {
        return DateModified;
    }

    public void setDateModified(String dateModified) {
        DateModified = dateModified;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }
}
