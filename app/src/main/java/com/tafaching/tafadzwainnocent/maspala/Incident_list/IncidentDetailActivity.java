package com.tafaching.tafadzwainnocent.maspala.Incident_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tafaching.tafadzwainnocent.maspala.CreateIncident.IncidentDetailsFragment;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.R;

public class IncidentDetailActivity extends Activity implements View.OnClickListener {
TextView incident_no,incident_location,incident_type,status,datetime,discription,top_bar;
ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_detail);
        IncidentListResponse incidentListResponse=(IncidentListResponse)getIntent().getSerializableExtra("details");
        incident_location=findViewById(R.id.maspala_location);
        incident_type=findViewById(R.id.incident_type);
        status=findViewById(R.id.status);
        incident_no=findViewById(R.id.incident_number);
        datetime=findViewById(R.id.date_time);
        imageView=findViewById(R.id.img_incident);
        top_bar=findViewById(R.id.top_bar);
        top_bar.setOnClickListener(this);
        discription=findViewById(R.id.incident_description);

        //set data
        incident_location.setText(""+incidentListResponse.getAddress());
        incident_type.setText(incidentListResponse.getIncidentType());
        status.setText("Status:"+incidentListResponse.getStatus());
        incident_no.setText("Reference#"+"\t"+ incidentListResponse.getReferenceNumber());
        datetime.setText(incidentListResponse.getDateRecieved());
        discription.setText(incidentListResponse.getCustomerComment());

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.top_bar:
                    Intent intents = new Intent(this, IncidentListFragment.class);
                    startActivity(intents);
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
