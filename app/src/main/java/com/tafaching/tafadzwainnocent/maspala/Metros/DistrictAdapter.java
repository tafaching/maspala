//package com.tafaching.tafadzwainnocent.maspala.Metros;
//
//import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.tafaching.tafadzwainnocent.maspala.R;
//
//import java.util.ArrayList;
//
//public class DistrictAdapter extends RecyclerView.Adapter<DistrictAdapter.ModulesViewHolder> {
//    private ArrayList<MetroResponse> metroResponse;
//    private Context context;
//    private String name = "District";
//    private OnItemClick onItemClick;
//
//    public class ModulesViewHolder extends RecyclerView.ViewHolder {
//        TextView textView;
//        ImageView imageView;
//        Button moredetails;
//        View currentView;
//
//        public ModulesViewHolder(@NonNull View itemView) {
//            super(itemView);
//            currentView = itemView;
//            textView = itemView.findViewById(R.id.mas_name);
//            imageView = itemView.findViewById(R.id.img_maspala);
//            moredetails = itemView.findViewById(R.id.more_details);
//        }
//    }
//
//    public DistrictAdapter(ArrayList<MetroResponse> metroResponse, Context context, OnItemClick onItemClick) {
//        this.metroResponse = metroResponse;
//        this.context = context;
//        this.onItemClick = onItemClick;
//    }
//
//    public void setMetroResponse(ArrayList<MetroResponse> metroResponse) {
//        this.metroResponse = metroResponse;
//        notifyDataSetChanged();
//    }
//
//    @NonNull
//    @Override
//    public ModulesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_maspala, viewGroup, false);
//        return new ModulesViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull ModulesViewHolder holder, int i) {
//       // if (name.equals(metroResponse.get(i).getOrganisationType())&& metroResponse.get(i).getName()!=null) {
//            holder.textView.setText(metroResponse.get(i).getName());
//        String url="http://154.0.165.41:1970/restapi/organisation/getorganisationlargelogo?filename=";
//        Glide.with(context)
//                .load(url+metroResponse.get(i).getOrganisationLibrary().getFileName())
//                .asBitmap()
//                .into(holder.imageView);
//
//            holder.currentView.setOnClickListener(v -> {
//                if ((onItemClick != null)) {
//                    onItemClick.onClickMetro(metroResponse.get(i));
//                }
//            });
//        //}
//    }
//
//    @Override
//    public int getItemCount() {
//        return metroResponse.size();
//    }
//
//    public interface OnItemClick {
//        void onClickMetro(final MetroResponse clickedResponse);
//    }
//}
