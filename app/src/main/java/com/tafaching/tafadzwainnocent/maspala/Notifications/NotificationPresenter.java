package com.tafaching.tafadzwainnocent.maspala.Notifications;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentListContract;
import com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentListRequest;
import com.tafaching.tafadzwainnocent.maspala.Network.Api;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NotificationPresenter implements NotificationContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static NotificationPresenter INSTANCE;
    private Api api;

    private NotificationPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    static NotificationPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new NotificationPresenter(context.getApplicationContext());
    }


    @Override
    public void requestNotificationList(NotificationRequest request, NotificationContract.NotificationView view) {
        api.getNotificationDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    view.onGetNotificationList(response);
                }, throwable -> {
                    view.onError(throwable.getMessage());
                });
    }
}
