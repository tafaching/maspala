package com.tafaching.tafadzwainnocent.maspala.Metros;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tafaching.tafadzwainnocent.maspala.Network.Api;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MetroPresenter implements MetroContract.Presenter {
    private Context context;
    private FragmentManager manager;
    private static MetroPresenter INSTANCE;
    private Api api;

    private MetroPresenter(Context context) {
        this.context = context;
        this.api = Api.start();
    }

    static MetroPresenter getInstance(Context context) {
        return INSTANCE != null ? INSTANCE : new MetroPresenter(context.getApplicationContext());
    }

    @Override
    public void requestOrg(final  MetroRequest request,MetroContract.MetroView view) {
        api.getOrg(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                            view.onGetOrg(response);
                        }, throwable -> {
                            view.onError(throwable.getMessage());
                        });
    }
}
