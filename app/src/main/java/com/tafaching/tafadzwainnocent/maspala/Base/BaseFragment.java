package com.tafaching.tafadzwainnocent.maspala.Base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment {

    protected ParentActivityObserver parentActivityObserver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getContentView(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setLayoutRef(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ParentActivityObserver) {
            parentActivityObserver = (ParentActivityObserver) context;
        }
    }

    @Override
    public void onDetach() {
        parentActivityObserver = null;
        super.onDetach();
    }

    public abstract int getContentView();

    protected abstract void setLayoutRef(View view, Bundle savedInstanceState);

    public interface ParentActivityObserver {
        void commit(BaseFragment baseFragment);
    }
}
