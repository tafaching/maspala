package com.tafaching.tafadzwainnocent.maspala.MoreDetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.Metros.MetroResponse;
import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.Util.Sharepref;

public class MoreDetailsActivity extends Activity implements MoreDetailsContract.MoreDetailsView, View.OnClickListener {
    TextView number, web, address, email;
    private Integer orgId=5;
    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;
    MoreDetailsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_details);
        presenter=MoreDetailsPresenter.getInstance(this);
        number = findViewById(R.id.maspala_tel);
        web = findViewById(R.id.maspala_website);
        address = findViewById(R.id.maspala_location);
        email = findViewById(R.id.maspala_email);
        toolbar_imgClose = findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = findViewById(R.id.toolbar_tvTitle);
        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar_imgClose.setOnClickListener(this);
        String xyz = "Contact Us";
        toolbar_tvTitle.setText(xyz);
//        post();
        details();


    }
    public void post(){
        MoreDetailsRequest request = new MoreDetailsRequest();
        request.setOrganisationId(orgId);
        presenter.requestOrgDetails(request, this);
    }
    public void details(){



    }

    @Override
    public void onGetOrgDetails(MetroResponse metroResponses) {
        if(metroResponses!=null){
            Toast.makeText(this, "Successful response!!", Toast.LENGTH_LONG).show();
        }
//        email.setText(metroResponses.getEmail());
//        web.setText(metroResponses.getWebsiteUrl());
//        address.setText(metroResponses.getPhysicalAddress());
//        number.setText(metroResponses.getPhoneNumber());

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.toolbar_imageview:
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
