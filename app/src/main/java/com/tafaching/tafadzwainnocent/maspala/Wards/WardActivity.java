package com.tafaching.tafadzwainnocent.maspala.Wards;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tafaching.tafadzwainnocent.maspala.Dashboard.DashboardActivity;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.Util.Sharepref;

import java.util.ArrayList;

public class WardActivity extends AppCompatActivity implements WardContract.WardView,WardAdapter.OnItemClick, View.OnClickListener {
    WardPresenter presenter;
    private RecyclerView recyclerView;
    WardAdapter wardAdapter;
    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;
    private ArrayList<WardResponse> wardResponses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ward);
        presenter = WardPresenter.getInstance(this);
        wardResponses = new ArrayList<>();
        recyclerView = findViewById(R.id.ward_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        wardAdapter = new WardAdapter(wardResponses,this, this::onClickMetro);
        toolbar_imgClose = findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = findViewById(R.id.toolbar_tvTitle);
        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar_imgClose.setOnClickListener(this);
        String xyz = "Ward Councillors";
        toolbar_tvTitle.setText(xyz);
        recyclerView.setAdapter(wardAdapter);
        post();
    }

    public void post() {
        Integer orgId = 5;
       // Integer orgId = Sharepref.getOrganisationId(this);
        WardRequest request = new WardRequest();
        request.setOrganisationId(orgId);
        presenter.requestWardList(request, this);
    }

    @Override
    public void onClickMetro(WardResponse clickedResponse) {
        Toast.makeText(this, "clicked!!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGetWardList(ArrayList<WardResponse> wardResponses) {
        if (wardResponses != null && wardResponses != null) {

            wardAdapter.setWardResponse(wardResponses);
            Toast.makeText(this, "Done", Toast.LENGTH_LONG).show();
            Log.e("Response List", wardResponses.toString());
        } else {
            Toast.makeText(this, "incident list  null!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, "Exception: " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.toolbar_imageview:
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }
}
