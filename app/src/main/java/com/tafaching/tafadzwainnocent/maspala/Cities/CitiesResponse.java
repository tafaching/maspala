package com.tafaching.tafadzwainnocent.maspala.Cities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CitiesResponse implements Serializable {
    @SerializedName("CityId")
    private Integer CityId;
    @SerializedName("Description")
    private String Description;
    @SerializedName("RegionId")
    private Integer RegionId;
    @SerializedName("CreatedByUser")
    private String CreatedByUser;
    @SerializedName("ModifiedByUser")
    private String ModifiedByUser;
    @SerializedName("CreatedBy")
    private String CreatedBy;
    @SerializedName("ModifiedBy")
    private String ModifiedBy;
    @SerializedName("DateCreated")
    private String DateCreated;
    @SerializedName("DateModified")
    private String DateModified;
    @SerializedName("Active")
    private Boolean Active;

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Integer getRegionId() {
        return RegionId;
    }

    public void setRegionId(Integer regionId) {
        RegionId = regionId;
    }

    public String getCreatedByUser() {
        return CreatedByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        CreatedByUser = createdByUser;
    }

    public String getModifiedByUser() {
        return ModifiedByUser;
    }

    public void setModifiedByUser(String modifiedByUser) {
        ModifiedByUser = modifiedByUser;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateModified() {
        return DateModified;
    }

    public void setDateModified(String dateModified) {
        DateModified = dateModified;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }
}
