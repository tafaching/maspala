package com.tafaching.tafadzwainnocent.maspala.Gender;


import java.util.ArrayList;

public interface GenderContract {

    interface View {

    }

    interface CenderView {
        void onSelectGender(ArrayList<GenderResponse> response);

        void onError(String message);
    }

    interface Presenter {

        void selectGender(GenderRequest request, CenderView view);
    }
}
