package com.tafaching.tafadzwainnocent.maspala.Wards;



import java.util.ArrayList;

public interface WardContract {
    interface View {

    }

    interface WardView {
        void onGetWardList(ArrayList<WardResponse> wardResponses);

        void onError(String message);
    }

    interface Presenter {
        void requestWardList(WardRequest request, WardView view);
    }
}
