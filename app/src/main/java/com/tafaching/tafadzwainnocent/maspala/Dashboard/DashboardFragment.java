package com.tafaching.tafadzwainnocent.maspala.Dashboard;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.CreateIncident.CreateIncidentActivity;
import com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentListFragment;
import com.tafaching.tafadzwainnocent.maspala.Main.MainActivity;
import com.tafaching.tafadzwainnocent.maspala.MoreDetails.MoreDetailsActivity;
import com.tafaching.tafadzwainnocent.maspala.Notifications.NoficationsFragment;
import com.tafaching.tafadzwainnocent.maspala.R;
import com.tafaching.tafadzwainnocent.maspala.TenderList.TenderActivity;
import com.tafaching.tafadzwainnocent.maspala.TrafficFinesLoginReg.TrafficActivity;
import com.tafaching.tafadzwainnocent.maspala.Wards.WardActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends BaseFragment implements View.OnClickListener {
    private CardView create,contact,my_incidents,notification,traffic,ward,tender;
    ImageView create_img,list_img,notification_img,img_traffic,img_ward,img_tender,img_contact;
    LinearLayout cont,container;
    TextView create_txt;
    private ImageView toolbar_imgClose;
    private TextView toolbar_tvTitle;

    @Override
    public int getContentView() {
        return R.layout.fragment_dashboard;
    }

    @Override
    protected void setLayoutRef(View view, Bundle savedInstanceState) {
        create = view.findViewById(R.id.card_create);
        create.setOnClickListener(this);
        cont=view.findViewById(R.id.container1);
        container=view.findViewById(R.id.container);
        contact = view.findViewById(R.id.card_contact_us);
        img_contact = view.findViewById(R.id.img_contact_us);
        img_contact.setOnClickListener(this);
        contact.setOnClickListener(this);
        my_incidents = view.findViewById(R.id.my_incidents);
        my_incidents.setOnClickListener(this);
        list_img = view.findViewById(R.id.img_my_incident);
        list_img.setOnClickListener(this);
        create_img = view.findViewById(R.id.img_create_incident);
        create_img.setOnClickListener(this);
        create_txt = view.findViewById(R.id.create_incident_txt);
        create_txt.setOnClickListener(this);
        notification_img = view.findViewById(R.id.img_notifications);
        notification_img.setOnClickListener(this);
        notification = view.findViewById(R.id.card_notification);
        notification.setOnClickListener(this);
//        img_traffic = view.findViewById(R.id.img_traffic_fines);
//        img_traffic.setOnClickListener(this);
//        traffic = view.findViewById(R.id.card_trafficfine);
//        traffic.setOnClickListener(this);
        img_ward = view.findViewById(R.id.img_ward_counsilors);
        img_ward.setOnClickListener(this);
        ward = view.findViewById(R.id.card_ward);
        ward.setOnClickListener(this);
//        img_tender = view.findViewById(R.id.img_tenders);
//        img_tender.setOnClickListener(this);
//        tender = view.findViewById(R.id.card_tenders);
 //       tender.setOnClickListener(this);
        toolbar_imgClose = view.findViewById(R.id.toolbar_imageview);
        toolbar_tvTitle = view.findViewById(R.id.toolbar_tvTitle);
//        toolbar_imgClose.setImageDrawable(getResources().getDrawable(R.drawable.menu));
//        toolbar_imgClose.setOnClickListener(this);
        String xyz = "The City of uMhlathuze";
        toolbar_tvTitle.setText(xyz);

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.toolbar_imageview:
//                    Intent intentk = new Intent(getActivity(), MainActivity.class);
//                    startActivity(intentk);
                    break;
                case R.id.card_create:
                    Intent intent = new Intent(getActivity(), CreateIncidentActivity.class);
                    startActivity(intent);
                    break;
                case R.id.img_create_incident:
                    Intent intents = new Intent(getActivity(), CreateIncidentActivity.class);
                    startActivity(intents);
                    break;
                case R.id.card_contact_us:
                    Intent intentx = new Intent(getActivity(), MoreDetailsActivity.class);
                    startActivity(intentx);
                    break;

                case R.id.img_contact_us:
                    Intent intentxx = new Intent(getActivity(), MoreDetailsActivity.class);
                    startActivity(intentxx);
                    break;
                case R.id.my_incidents:

                    Intent intentss = new Intent(getActivity(), IncidentListFragment.class);
                    startActivity(intentss);
                    break;
                case R.id.img_my_incident:

                    Intent intentas = new Intent(getActivity(), IncidentListFragment.class);
                    startActivity(intentas);
                    break;

                case R.id.card_notification:
                    parentActivityObserver.commit(new NoficationsFragment());
                    break;
                case R.id.img_notifications:
                   parentActivityObserver.commit(new NoficationsFragment());
                    break;

//                case R.id.card_trafficfine:
//                    Intent intentt = new Intent(getActivity(), TrafficActivity.class);
//                    startActivity(intentt);
//                    break;
//                case R.id.img_traffic_fines:
//                    Intent intentts = new Intent(getActivity(), TrafficActivity.class);
//                    startActivity(intentts);
//                    break;

                case R.id.card_ward:
                    Intent intentw = new Intent(getActivity(), WardActivity.class);
                    startActivity(intentw);
                    break;
                case R.id.img_ward_counsilors:
                    Intent intentws = new Intent(getActivity(), WardActivity.class);
                    startActivity(intentws);
                    break;

//                case R.id.card_tenders:
//                    Intent intentq = new Intent(getActivity(), TenderActivity.class);
//                    startActivity(intentq);
//                    break;
//                case R.id.img_tenders:
//                    Intent intentqs = new Intent(getActivity(), TenderActivity.class);
//                    startActivity(intentqs);
//                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
