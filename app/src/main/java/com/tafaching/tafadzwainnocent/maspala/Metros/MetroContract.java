package com.tafaching.tafadzwainnocent.maspala.Metros;

import java.util.ArrayList;

public interface MetroContract {
    interface View {

    }

    interface MetroView {
        void onGetOrg(ArrayList<MetroResponse> metroResponses);

        void onError(String message);
    }

    interface Presenter {
        void requestOrg(MetroRequest request, MetroView view);
    }
}
