package com.tafaching.tafadzwainnocent.maspala.Wards;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.tafaching.tafadzwainnocent.maspala.R;

import java.util.ArrayList;

public class WardAdapter extends RecyclerView.Adapter<WardAdapter.WardViewHolder> {
    private ArrayList<WardResponse> wardResponse;
    private Context context;
    private WardAdapter.OnItemClick onItemClick;

    public class WardViewHolder extends RecyclerView.ViewHolder {
        TextView title,name,ward,phone,email;
        ImageView imageView;

        View currentView;

        public WardViewHolder(@NonNull View itemView) {
            super(itemView);
            currentView = itemView;
            title = itemView.findViewById(R.id.municipal_name);
            name = itemView.findViewById(R.id.councilor_name);
            ward = itemView.findViewById(R.id.ward);
            phone = itemView.findViewById(R.id.councilor_phone);
            email = itemView.findViewById(R.id.councilor_email);
            imageView=itemView.findViewById(R.id.profile);

        }
    }

    public WardAdapter(ArrayList<WardResponse> wardResponse, Context context, WardAdapter.OnItemClick onItemClick) {
        this.wardResponse = wardResponse;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    public void setWardResponse(ArrayList<WardResponse> wardResponse) {
        this.wardResponse = wardResponse;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WardAdapter.WardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_ward, viewGroup, false);
        return new WardAdapter.WardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WardAdapter.WardViewHolder holder, int i) {

        holder.title.setText(wardResponse.get(i).getOrganisation());
        holder.name.setText(wardResponse.get(i).getContactName());
        holder.ward.setText(wardResponse.get(i).getWard());
        holder.email.setText(wardResponse.get(i).getContactEmail());
        holder.phone.setText(wardResponse.get(i).getContactPhone());
        String url="http://154.0.165.41:1970/restapi/organisation/getcouncillorlargelogo?filename=";
        Glide.with(context)
                .load(url+wardResponse.get(i).getCouncillorLibrary().getFileName())
                .asBitmap()
                .into(holder.imageView);
        holder.currentView.setOnClickListener(v -> {
            if ((onItemClick != null)) {
                onItemClick.onClickMetro(wardResponse.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return wardResponse.size();
    }

    public interface OnItemClick {
        void onClickMetro(final WardResponse clickedResponse);
    }
}
