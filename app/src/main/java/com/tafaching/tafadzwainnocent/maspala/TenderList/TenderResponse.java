package com.tafaching.tafadzwainnocent.maspala.TenderList;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class TenderResponse implements Serializable {

    @SerializedName("TenderId")
    private Integer  TenderId;
    @SerializedName("TenderNo")
    private String  TenderNo;
    @SerializedName("TenderCategoryId")
    private Integer  TenderCategoryId;
    @SerializedName("Category")
    private String  Category;
    @SerializedName("BriefingId")
    private Integer  BriefingId;
    @SerializedName("Briefing")
    private String  Briefing;
    @SerializedName("OrganisationId")
    private Integer  OrganisationId;
    @SerializedName("Organisation")
    private String  Organisation;
    @SerializedName("SoftCopyId")
    private Integer  SoftCopyId;
    @SerializedName("SoftCopy")
    private String  SoftCopy;
    @SerializedName("ShortDescription")
    private String  ShortDescription;
    @SerializedName("Description")
    private String  Description;
    @SerializedName("DatePosted")
    private String  DatePosted;
    @SerializedName("ClosingDate")
    private String  ClosingDate;
    @SerializedName("Time")
    private String  Time;
    @SerializedName("Venue")
    private String  Venue;
    @SerializedName("ProvinceId")
    private Integer  ProvinceId;
    @SerializedName("Province")
    private String  Province;
    @SerializedName("RegionId")
    private Integer  RegionId;
    @SerializedName("Region")
    private String  Region;
    @SerializedName("CityId")
    private Integer  CityId;
    @SerializedName("City")
    private String  City;
    @SerializedName("SuburbId")
    private Integer  SuburbId;
    @SerializedName("Suburb")
    private String  Suburb;
    @SerializedName("DocumentPrice")
    private String  DocumentPrice;
    @SerializedName("CreatedBy")
    private String  CreatedBy;
    @SerializedName("ModifiedBy")
    private String  ModifiedBy;
    @SerializedName("DateCreated")
    private String  DateCreated;
    @SerializedName("DateModified")
    private String  DateModified;
    @SerializedName("Active")
    private Boolean  Active;
    @SerializedName("CreatedByUser")
    private String  CreatedByUser;
    @SerializedName("ModifiedByUser")
    private String  ModifiedByUser;
    @SerializedName("TenderDocuments")
    private ArrayList<TenderDocuments> tenderDocuments;
    @SerializedName("FileAttachments")
    private String  FileAttachments;
    @SerializedName("Bytes")
    private String  Bytes;
    @SerializedName("Contact")
    private String  Contact;
    @SerializedName("Contacts")
    private ArrayList<Contacts> contacts;

    public Integer getTenderId() {
        return TenderId;
    }

    public void setTenderId(Integer tenderId) {
        TenderId = tenderId;
    }

    public String getTenderNo() {
        return TenderNo;
    }

    public void setTenderNo(String tenderNo) {
        TenderNo = tenderNo;
    }

    public Integer getTenderCategoryId() {
        return TenderCategoryId;
    }

    public void setTenderCategoryId(Integer tenderCategoryId) {
        TenderCategoryId = tenderCategoryId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public Integer getBriefingId() {
        return BriefingId;
    }

    public void setBriefingId(Integer briefingId) {
        BriefingId = briefingId;
    }

    public String getBriefing() {
        return Briefing;
    }

    public void setBriefing(String briefing) {
        Briefing = briefing;
    }

    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer organisationId) {
        OrganisationId = organisationId;
    }

    public String getOrganisation() {
        return Organisation;
    }

    public void setOrganisation(String organisation) {
        Organisation = organisation;
    }

    public Integer getSoftCopyId() {
        return SoftCopyId;
    }

    public void setSoftCopyId(Integer softCopyId) {
        SoftCopyId = softCopyId;
    }

    public String getSoftCopy() {
        return SoftCopy;
    }

    public void setSoftCopy(String softCopy) {
        SoftCopy = softCopy;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDatePosted() {
        return DatePosted;
    }

    public void setDatePosted(String datePosted) {
        DatePosted = datePosted;
    }

    public String getClosingDate() {
        return ClosingDate;
    }

    public void setClosingDate(String closingDate) {
        ClosingDate = closingDate;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getVenue() {
        return Venue;
    }

    public void setVenue(String venue) {
        Venue = venue;
    }

    public Integer getProvinceId() {
        return ProvinceId;
    }

    public void setProvinceId(Integer provinceId) {
        ProvinceId = provinceId;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public Integer getRegionId() {
        return RegionId;
    }

    public void setRegionId(Integer regionId) {
        RegionId = regionId;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public Integer getSuburbId() {
        return SuburbId;
    }

    public void setSuburbId(Integer suburbId) {
        SuburbId = suburbId;
    }

    public String getSuburb() {
        return Suburb;
    }

    public void setSuburb(String suburb) {
        Suburb = suburb;
    }

    public String getDocumentPrice() {
        return DocumentPrice;
    }

    public void setDocumentPrice(String documentPrice) {
        DocumentPrice = documentPrice;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(String dateCreated) {
        DateCreated = dateCreated;
    }

    public String getDateModified() {
        return DateModified;
    }

    public void setDateModified(String dateModified) {
        DateModified = dateModified;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public String getCreatedByUser() {
        return CreatedByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        CreatedByUser = createdByUser;
    }

    public String getModifiedByUser() {
        return ModifiedByUser;
    }

    public void setModifiedByUser(String modifiedByUser) {
        ModifiedByUser = modifiedByUser;
    }

    public ArrayList<TenderDocuments> getTenderDocuments() {
        return tenderDocuments;
    }

    public void setTenderDocuments(ArrayList<TenderDocuments> tenderDocuments) {
        this.tenderDocuments = tenderDocuments;
    }

    public String getFileAttachments() {
        return FileAttachments;
    }

    public void setFileAttachments(String fileAttachments) {
        FileAttachments = fileAttachments;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String bytes) {
        Bytes = bytes;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public ArrayList<Contacts> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contacts> contacts) {
        this.contacts = contacts;
    }
}
