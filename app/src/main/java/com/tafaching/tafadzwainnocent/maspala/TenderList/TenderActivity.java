package com.tafaching.tafadzwainnocent.maspala.TenderList;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.tafaching.tafadzwainnocent.maspala.Base.BaseFragment;
import com.tafaching.tafadzwainnocent.maspala.R;

public class TenderActivity extends AppCompatActivity implements  BaseFragment.ParentActivityObserver {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tender);
        commit(new TenderFragment());
    }

    @Override
    public void commit(BaseFragment baseFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, baseFragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

}
