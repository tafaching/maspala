package com.tafaching.tafadzwainnocent.maspala.Network;
import com.tafaching.tafadzwainnocent.maspala.Cities.CitiesResponse;
import com.tafaching.tafadzwainnocent.maspala.CreateIncident.CreateIncidentRequest;
import com.tafaching.tafadzwainnocent.maspala.CreateIncident.CreateIncidentResponse;
import com.tafaching.tafadzwainnocent.maspala.Gender.GenderResponse;
import com.tafaching.tafadzwainnocent.maspala.IncidentTypes.TypesResponse;
import com.tafaching.tafadzwainnocent.maspala.Incident_list.IncidentListResponse;
import com.tafaching.tafadzwainnocent.maspala.Metros.MetroResponse;
import com.tafaching.tafadzwainnocent.maspala.Notifications.NotificationResponse;
import com.tafaching.tafadzwainnocent.maspala.Suburbs.SuburbResponse;
import com.tafaching.tafadzwainnocent.maspala.TenderList.TenderResponse;
import com.tafaching.tafadzwainnocent.maspala.Wards.WardResponse;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface Service {

    // Create Incident
    @Headers("Content-Type: application/json")

    @POST("incidents/createmobileincident")
    Observable<Integer> createIncident(@Body CreateIncidentRequest request);
    // Get ALl Organisations
    @GET("organisation/{id}")
    Observable<ArrayList<MetroResponse>> getOrganisations(@Path("id") String org);


    // Get  Organisations Details
    @GET("organisation/getorganisationdetails/{id}")
    Observable<MetroResponse> getOrgDetails(@Path("id") Integer org);

    // Get incident by phone number
    @GET("incidents/incidents/{id}")
    Observable<ArrayList<IncidentListResponse>> getIncList(@Path("id") String mobileNo);

    //Get notifications by id

    @GET("settings/getpublications/{id}")
    Observable<ArrayList<NotificationResponse>> getNotificationList(@Path("id") Integer orgId);

    //Get counscilors
    @GET("organisation/getcouncillors/{id}")
    Observable<ArrayList<WardResponse>> getWardList(@Path("id") Integer orgId);

    //Get tender list
    @GET("tenders/tenders/{id}")
    Observable<ArrayList<TenderResponse>> getTenderList(@Path("id") Integer orgId);

    //Get tender list
    @GET("settings/{id}")
    Observable<ArrayList<GenderResponse>> getGenderList(@Path("id") String genders);

    //Get suburbs list
    @GET("settings/{id}")
    Observable<ArrayList<SuburbResponse>> getSuburbList(@Path("id") String suburbs);


    //Get cities list
    @GET("settings/{id}")
    Observable<ArrayList<CitiesResponse>> getCitiesList(@Path("id") String cities);

    //Get type list
    @GET("settings/{id}")
    Observable<ArrayList<TypesResponse>> getTypeList(@Path("id") String type);
}
