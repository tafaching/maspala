package com.tafaching.tafadzwainnocent.maspala.IncidentTypes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TypesRequest implements Serializable {
    @SerializedName("incidenttypes")
    private String incidenttypes;

    public String getIncidenttypes() {
        return incidenttypes;
    }

    public void setIncidenttypes(String incidenttypes) {
        this.incidenttypes = incidenttypes;
    }
}
