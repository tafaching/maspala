package com.tafaching.tafadzwainnocent.maspala.Incident_list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.tafaching.tafadzwainnocent.maspala.R;

import java.util.ArrayList;

public class IncidentListAdapter extends RecyclerView.Adapter<IncidentListAdapter.IncidentViewHolder> {
    private ArrayList<IncidentListResponse> incidentListResponse;
    private Context context;
    private IncidentListAdapter.OnItemClick onItemClick;

    public class IncidentViewHolder extends RecyclerView.ViewHolder {
        TextView incident_no, incident_title, incident_descrip, incident_date, incident_time;
        ImageView imageView;
        Button incident_status;
        View currentView;

        public IncidentViewHolder(@NonNull View itemView) {
            super(itemView);
            currentView = itemView;
            incident_no = itemView.findViewById(R.id.incident_no);
            incident_title = itemView.findViewById(R.id.incident_type);
            incident_descrip = itemView.findViewById(R.id.incident_description);
            incident_date = itemView.findViewById(R.id.incident_date);
            incident_time = itemView.findViewById(R.id.incident_time);
            incident_status = itemView.findViewById(R.id.status);
            //imageView = itemView.findViewById(R.id.status);

        }
    }

    public IncidentListAdapter(ArrayList<IncidentListResponse> incidentListResponse, Context context, IncidentListAdapter.OnItemClick onItemClick) {
        this.incidentListResponse = incidentListResponse;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    public void setIncidentListResponse(ArrayList<IncidentListResponse> incidentListResponse) {
        this.incidentListResponse = incidentListResponse;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public IncidentListAdapter.IncidentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_incident_list, viewGroup, false);
        return new IncidentListAdapter.IncidentViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull IncidentListAdapter.IncidentViewHolder holder, int i) {

        holder.incident_no.setText(incidentListResponse.get(i).getReferenceNumber());
        holder.incident_title.setText(incidentListResponse.get(i).getIncidentType());
        holder.incident_descrip.setText(incidentListResponse.get(i).getCustomerComment());
        holder.incident_time.setText(incidentListResponse.get(i).getTimeRecieved());
        holder.incident_date.setText(incidentListResponse.get(i).getDateRecieved());
        if(incidentListResponse.get(i).getStatus().equals("Pending")){
            holder.incident_status.setBackgroundColor(Color.BLUE);
            holder.incident_status.setText(incidentListResponse.get(i).getStatus());
        }
        if(incidentListResponse.get(i).getStatus().equals("Assigned")){
            holder.incident_status.setBackgroundColor(Color.BLACK);
            holder.incident_status.setText(incidentListResponse.get(i).getStatus());
        }
        if(incidentListResponse.get(i).getStatus().equals("New")){
            holder.incident_status.setBackgroundColor(Color.RED);
            holder.incident_status.setText(incidentListResponse.get(i).getStatus());
        }

        holder.currentView.setOnClickListener(v -> {
            if ((onItemClick != null)) {
                onItemClick.onClickMetro(incidentListResponse.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return incidentListResponse.size();
    }

    public interface OnItemClick {
        void onClickMetro(final IncidentListResponse clickedResponse);
    }
}
