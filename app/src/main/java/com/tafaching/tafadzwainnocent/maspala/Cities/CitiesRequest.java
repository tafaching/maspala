package com.tafaching.tafadzwainnocent.maspala.Cities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CitiesRequest implements Serializable {
    @SerializedName("cities")
    private String cities;

    public String getCities() {
        return cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }
}
