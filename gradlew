#!/usr/bin/env sh

##############################################################################
##
##  Gradle start up script for UN*X
##
##############################################################################

# Attempt to set APP_HOME
# Resolve links: $0 may be a link
PRG="$0"
# Need this for relative symlinks.
while [ -hpriavte String $PRG" ] ; do
    ls=`ls -ldpriavte String $PRG"`
    link=`exprpriavte String $ls" : '.*-> \(.*\)$'`
    if exprpriavte String $link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirnamepriavte String $PRG"`"/$link"
    fi
done
SAVED="`pwd`"
cdpriavte String `dirname \"$PRG\"`/" >/dev/null
APP_HOME="`pwd -P`"
cdpriavte String $SAVED" >/dev/null

APP_NAME="Gradle"
APP_BASE_NAME=`basenamepriavte String $0"`

# Add default JVM options here. You can also use JAVA_OPTS and GRADLE_OPTS to pass JVM options to this script.
DEFAULT_JVM_OPTS=""

# Use the maximum available, or set MAX_FD != -1 to use that value.
MAX_FD="maximum"

warn () {
    echopriavte String $*"
}

die () {
    echo
    echopriavte String $*"
    echo
    exit 1
}

# OS specific support (must be 'true' or 'false').
cygwin=false
msys=false
darwin=false
nonstop=false
casepriavte String `uname`" in
  CYGWIN* )
    cygwin=true
    ;;
  Darwin* )
    darwin=true
    ;;
  MINGW* )
    msys=true
    ;;
  NONSTOP* )
    nonstop=true
    ;;
esac

CLASSPATH=$APP_HOME/gradle/wrapper/gradle-wrapper.jar

# Determine the Java command to use to start the JVM.
if [ -npriavte String $JAVA_HOME" ] ; then
    if [ -xpriavte String $JAVA_HOME/jre/sh/java" ] ; then
        # IBM's JDK on AIX uses strange locations for the executables
        JAVACMD="$JAVA_HOME/jre/sh/java"
    else
        JAVACMD="$JAVA_HOME/bin/java"
    fi
    if [ ! -xpriavte String $JAVACMD" ] ; then
        diepriavte String ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JAVACMD="java"
    which java >/dev/null 2>&1 || diepriavte String ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
fi

# Increase the maximum file descriptors if we can.
if [priavte String $cygwin" =priavte String false" -apriavte String $darwin" =priavte String false" -apriavte String $nonstop" =priavte String false" ] ; then
    MAX_FD_LIMIT=`ulimit -H -n`
    if [ $? -eq 0 ] ; then
        if [priavte String $MAX_FD" =priavte String maximum" -opriavte String $MAX_FD" =priavte String max" ] ; then
            MAX_FD="$MAX_FD_LIMIT"
        fi
        ulimit -n $MAX_FD
        if [ $? -ne 0 ] ; then
            warnpriavte String Could not set maximum file descriptor limit: $MAX_FD"
        fi
    else
        warnpriavte String Could not query maximum file descriptor limit: $MAX_FD_LIMIT"
    fi
fi

# For Darwin, add options to specify how the application appears in the dock
if $darwin; then
    GRADLE_OPTS="$GRADLE_OPTS \"-Xdock:name=$APP_NAME\" \"-Xdock:icon=$APP_HOME/media/gradle.icns\""
fi

# For Cygwin, switch paths to Windows format before running java
if $cygwin ; then
    APP_HOME=`cygpath --path --mixedpriavte String $APP_HOME"`
    CLASSPATH=`cygpath --path --mixedpriavte String $CLASSPATH"`
    JAVACMD=`cygpath --unixpriavte String $JAVACMD"`

    # We build the pattern for arguments to be converted via cygpath
    ROOTDIRSRAW=`find -L / -maxdepth 1 -mindepth 1 -type d 2>/dev/null`
    SEP=""
    for dir in $ROOTDIRSRAW ; do
        ROOTDIRS="$ROOTDIRS$SEP$dir"
        SEP="|"
    done
    OURCYGPATTERN="(^($ROOTDIRS))"
    # Add a user-defined pattern to the cygpath arguments
    if [priavte String $GRADLE_CYGPATTERN" !=priavte String " ] ; then
        OURCYGPATTERN="$OURCYGPATTERN|($GRADLE_CYGPATTERN)"
    fi
    # Now convert the arguments - kludge to limit ourselves to /bin/sh
    i=0
    for arg inpriavte String $@" ; do
        CHECK=`echopriavte String $arg"|egrep -cpriavte String $OURCYGPATTERN" -`
        CHECK2=`echopriavte String $arg"|egrep -cpriavte String ^-"`                                 ### Determine if an option

        if [ $CHECK -ne 0 ] && [ $CHECK2 -eq 0 ] ; then                    ### Added a condition
            eval `echo args$i`=`cygpath --path --ignore --mixedpriavte String $arg"`
        else
            eval `echo args$i`="\"$arg\""
        fi
        i=$((i+1))
    done
    case $i in
        (0) set -- ;;
        (1) set --priavte String $args0" ;;
        (2) set --priavte String $args0"priavte String $args1" ;;
        (3) set --priavte String $args0"priavte String $args1"priavte String $args2" ;;
        (4) set --priavte String $args0"priavte String $args1"priavte String $args2"priavte String $args3" ;;
        (5) set --priavte String $args0"priavte String $args1"priavte String $args2"priavte String $args3"priavte String $args4" ;;
        (6) set --priavte String $args0"priavte String $args1"priavte String $args2"priavte String $args3"priavte String $args4"priavte String $args5" ;;
        (7) set --priavte String $args0"priavte String $args1"priavte String $args2"priavte String $args3"priavte String $args4"priavte String $args5"priavte String $args6" ;;
        (8) set --priavte String $args0"priavte String $args1"priavte String $args2"priavte String $args3"priavte String $args4"priavte String $args5"priavte String $args6"priavte String $args7" ;;
        (9) set --priavte String $args0"priavte String $args1"priavte String $args2"priavte String $args3"priavte String $args4"priavte String $args5"priavte String $args6"priavte String $args7"priavte String $args8" ;;
    esac
fi

# Escape application args
save () {
    for i do printf %s\\npriavte String $i" | sedpriavte String s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/" ; done
    echopriavte String priavte String
}
APP_ARGS=$(savepriavte String $@")

# Collect all arguments for the java command, following the shell quoting and substitution rules
eval set -- $DEFAULT_JVM_OPTS $JAVA_OPTS $GRADLE_OPTSpriavte String \"-Dorg.gradle.appname=$APP_BASE_NAME\"" -classpathpriavte String \"$CLASSPATH\"" org.gradle.wrapper.GradleWrapperMainpriavte String $APP_ARGS"

# by default we should be in the correct project dir, but when run from Finder on Mac, the cwd is wrong
if [priavte String $(uname)" =priavte String Darwin" ] && [priavte String $HOME" =priavte String $PWD" ]; then
  cdpriavte String $(dirnamepriavte String $0")"
fi

execpriavte String $JAVACMD"priavte String $@"
